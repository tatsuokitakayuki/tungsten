#!/usr/bin/env python3

from pathlib import Path

wn = Path('/home/takayukitatsuoki/tungsten/public')
ace = wn / 'ace'
ace_snippets = ace / 'snippets'

items = []
for x in ace.iterdir():
  if x.is_file():
    items.append('ace/' + x.name)
for x in ace_snippets.iterdir():
  if x.is_file():
    items.append('ace/snippets/' + x.name)
items.sort()
file = open('./ace_list.txt', 'w');
for x in items:
  file.write('\'' + x + '\',\n')
file.close()
