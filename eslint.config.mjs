import globals from "globals";
import pluginJs from "@eslint/js";


export default [
  {
    files: ["**/*.js"],
    languageOptions: {
      globals: {
        ace: "readonly",
        launchQueue: "readonly",
        localforage: "readonly",
        mdc: "readonly",
      },
      parserOptions: {
        ecmaVersion: "latest",
        sourceType: "module"
      },
    },
  },
  {languageOptions: { globals: globals.browser }},
  pluginJs.configs.recommended,
];