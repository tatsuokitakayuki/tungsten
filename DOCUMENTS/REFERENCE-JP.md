# Tungsten Text Editor リファレンス

## キーボードショートカット

* US 配列のキー配列でまとめている

### Line operations

| 動作 | Windows/Linux/ChromeOS | Mac | 説明 |
|:-----------|:-----------|:-----------|:-----------|
| Remove line | Ctrl+D | Cmd+D | 行削除 |
| Copy lines up | Alt+Shift+Up | Cmd+Opt+Up | 上方向に行コピー |
| Copy lines down | Alt+Shift+Down | Cmd+Opt+Down | 下方向に行コピー |
| Move lines up | Alt+Up | Opt+Up | 上方向に行移動 |
| Move lines down | Alt+Down | Opt+Down | 下方向に行移動 |
| Remove to line start | Alt+Backspace | Cmd+Backspace | 行頭まで削除 |
| Remove to line end | Alt+Delete | Ctrl+K, Cmd+Delete | 行末まで削除 |
| Remove to line start hard | Ctrl+Shift+Backspace | 未定義 | 行頭まで削除 |
| Remove to line end hard | Ctrl+Shift+Delete | 未定義 | 行末まで削除 |
| Remove word left | Ctrl+Backspace | Opt+Backspace, Ctrl+Opt+Backspace | 左の単語を削除 |
| Remove word right | Ctrl+Delete | Opt+Delete | 右の単語を削除 |
| Split line | 未定義 | Ctrl+O | 行を分ける |

### Selection

| 動作 | Windows/Linux/ChromeOS | Mac | 説明 |
|:-----------|:-----------|:-----------|:-----------|
| Select all | Ctrl+A | Cmd+A | 全選択 |
| Select left | Shift+Left | Shift+Left, Ctrl+Shift+B | 左を選択 |
| Select right | Shift+Right | Shift+Right | 右を選択 |
| Select word left | Ctrl+Shift+Left | Opt+Shift+Left | 左の単語を選択 |
| Select word right | Ctrl+Shift+Right | Opt+Shift+Right | 右の単語を選択 |
| Select line start | Shift+Home | Shift+Home | 行頭まで選択 |
| Select line end | Shift+End | Shift+End | 行末まで選択 |
| Select to line start | Alt+Shift+Left | Cmd+Shift+Left, Ctrl+Shift+A | 行末まで選択 |
| Select to line end | Alt+Shift+Right | Cmd+Shift+Right, Ctrl+Shift+E | 行頭まで選択 |
| Select up | Shift+Up | Shift+Up, Ctrl+Shift+P | 上まで選択 |
| Select down | Shift+Down | Shift+Down, Ctrl+Shift+N | 下まで選択 |
| Select page up | Shift+PageUp | Shift+PageUp | 上ページを選択 |
| Select page down | Shift+PageDown | Shift+PageDown | 下ページを選択 |
| Select to start | Ctrl+Shift+Home | Cmd+Shift+Home, Cmd+Shift+Up | 先頭まで選択 |
| Select to end | Ctrl+Shift+End | Cmd+Shift+End, Cmd+Shift+Down | 末尾まで選択 |
| Duplicate selection | Ctrl+Shift+D | Cmd+Shift+D | 選択中範囲を複製 |
| Select to matching | Ctrl+Shift+\\, Ctrl+Shift+P | Cmd+Shift+\\ |  |
| Expand to matching | Ctrl+Shift+M | Ctrl+Shift+M |  |

### Multi-cursor

| 動作 | Windows/Linux/ChromeOS | Mac | 説明 |
|:-----------|:-----------|:-----------|:-----------|
| Add cursor above | Ctrl+Alt+Up | Ctrl+Opt+Up | カーソル上1行移動してカーソルを追加 |
| Add cursor below | Ctrl+Alt+Down | Ctrl+Opt+Down | カーソル下1行移動してカーソルを追加 |
| Add next occurrence to multi-selection | Ctrl+Alt+Right | Ctrl+Opt+Right | 次の単語をマルチセレクトに追加 |
| Add previous occurrence to multi-selection | Ctrl+Alt+Left | Ctrl+Opt+Left | 前の単語をマルチセレクトに追加 |
| Move multi-cursor from current line to the line above | Ctrl+Alt+Shift+Up | Ctrl+Opt+Shift+Up | マルチカーソルをカーソル上1行移動 |
| Move multi-cursor from current line to the line below | Ctrl+Alt+Shift+Down | Ctrl+Opt+Shift+Down | マルチカーソルをカーソル下1行移動 |
| Remove current occurrence from multi-selection and move to next | Ctrl+Alt+Shift+Right | Ctrl+Opt+Shift+Right | 次の単語をマルチセレクトから除去 |
| Remove current occurrence from multi-selection and move to previous | Ctrl+Alt+Shift+Left | Ctrl+Opt+Shift+Left | 前の単語をマルチセレクトから除去 |
| Select all from multi-selection (Expand to line) | Ctrl+Shift+L | Ctrl+Shift+L | マルチセレクトをすべて選択 |
| Align cursors | Ctrl+Alt+A | Ctrl+Opt+A |  |

### Go to

| 動作 | Windows/Linux/ChromeOS | Mac | 説明 |
|:-----------|:-----------|:-----------|:-----------|
| Go to left | Left | Left, Ctrl+B | 左に1文字移動 |
| Go to right | Right | Right, Ctrl+F | 右に1文字移動 |
| Go to word left | Ctrl+Left | Opt+Left | 左に1単語移動 |
| Go to word right | Ctrl+Right | Opt+Right | 右に1単語移動 |
| Go line up | Up | Up, Ctrl+P | 上に1行移動 |
| Go line down | Down | Down, Ctrl+N | 下に1行移動 |
| Go to line start | Alt+Left, Home | Cmd+Left, Home, Ctrl+A | 行頭に移動 |
| Go to line end | Alt+Right, End | Cmd+Right, End, Ctrl+E | 行末に移動 |
| Go to page up | PageUp | PageUp | 1画面上に移動 |
| Go to page down | PageDown | PageDown, Ctrl+V | 1画面下に移動 |
| Go to start | Ctrl+Home | Cmd+Home, Cmd+Up | 先頭に移動 |
| Go to end | Ctrl+End | Cmd+End, Cmd+Down | 末尾に移動 |
| Go to line | Ctrl+L | Cmd+L | 移動先を数値で指定して移動 |
| Scroll up | Ctrl+Up | 未定義 | カーソルを動かさず上スクロール |
| Scroll down | Ctrl+Down | 未定義 | カーソルを動かさず下スクロール |
| Page up | 未定義 | Opt+PageUp | カーソルを動かさず上画面スクロール |
| Page down | 未定義 | Opt+PageDown | カーソルを動かさず下画面スクロール |
| Jump to matching | Ctrl+\\, Ctrl+P | Cmd+\\ |  |
| Go to next error | Alt+E | F4 | 次のエラーに移動 |
| Go to previous error | Alt+Shift+E | Shift+F4 | 前のエラーに移動 |

### Find/Replace

| 動作 | Windows/Linux/ChromeOS | Mac | 説明 |
|:-----------|:-----------|:-----------|:-----------|
| Find | Ctrl+F | Cmd+F | 検索 |
| Replace | Ctrl+H | Cmd+Opt+F | 置換 |
| Find next | Ctrl+K | Cmd+G | 次を検索 |
| Find Previous | Ctrl+Shift+K | Cmd+Shift+G | 前を検索 |
| Find all | Ctrl+Alt+K | 未定義 | 全て検索 |
| Select or find next | Alt+K | Ctrl+G | 次を検索 |
| Select or find previous | Alt+Shift+K | Ctrl+Shift+G | 前を検索 |

### Folding

| 動作 | Windows/Linux/ChromeOS | Mac | 説明 |
|:-----------|:-----------|:-----------|:-----------|
| Fold selection | Alt+L, Ctrl+F1 | Cmd+Opt+L, Cmd+F1 | 選択箇所を畳む |
| Unfold | Alt+Shift+L, Ctrl+Shift+F1 | Cmd+Opt+Shift+L, Cmd+Shift+F1 | カーソル位置を広げる |
| Fold all | 未定義 | Ctrl+Cmd+Opt+0 | 全て畳む |
| Fold other | Alt+0 | Cmd+Opt+0 | カーソル位置以外を畳む |
| Unfold all | Alt+Shift+0 | Cmd+Opt+Shift+0 | 全て広げる |
| Fold all comments | 未定義 | Ctrl+Cmd+Opt+0 | 全てのコメントを畳む |
| Toggle fold widget | F2 | F2 | カーソル位置を畳む・広げる |
| Toggle parent fold widget | Alt+F2 | Opt+F2 | 親を畳む・広げる |

### Help

| 動作 | Windows/Linux/ChromeOS | Mac | 説明 |
|:-----------|:-----------|:-----------|:-----------|
| Command palette | F1 | 未定義 | ファイルパレットを開く |
| Show settings menu | Ctrl+, | Cmd+, | セッティングメニューを開く |
| Show keyboard shortcuts | Ctrl+Alt+H | Cmd+Opt+H | キーボードショートカットを表示 |

### File

| 動作 | Windows/Linux/ChromeOS | Mac | 説明 |
|:-----------|:-----------|:-----------|:-----------|
| New file | Ctrl+N | Cmd+N | 新しいファイル |
| Open file | Ctrl+O | Cmd+O | 既存のファイルを開く |
| Save file | Ctrl+S | Cmd+S | ファイルを保存 |
| Rename file | Alt+R | Opt+R | ファイル名を変える |
| Close file | Alt+W | Opt+W | ファイルを閉じる |
| Quit app | Ctrl+W | Cmd+W | アプリの終了 |

### File list

| 動作 | Windows/Linux/ChromeOS | Mac | 説明 |
|:-----------|:-----------|:-----------|:-----------|
| Toggle file list | Ctrl+Shift+F | 未定義 | ファイルリストの開閉 |
| Next file | Ctrl+Tab | 未定義 | 次のファイルを選択 |
| Previous file | Ctrl+Shift+Tab | 未定義 | 前のファイルを選択 |
| Select 1st to 8th file | Ctrl+1~8 | 未定義 | 1~8番目のファイルを選択 |
| Select first file | Ctrl+0 | 未定義 | 最初のファイルを選択 |
| Select last file | Ctrl+9 | 未定義 | 最後のファイルを選択 |

### Other

| 動作 | Windows/Linux/ChromeOS | Mac | 説明 |
|:-----------|:-----------|:-----------|:-----------|
| Cut | Ctrl+X | Cmd+X | カット |
| Copy | Ctrl+C | Cmd+C | コピー |
| Paste | Ctrl+V | Cmd+V | ペースト |
| Indent | Tab | Tab | 1段階インデントを付ける |
| Outdent | Shift+Tab | Shift+Tab | 1段階インデントを取り除く |
| Block indent | Ctrl+] | 未定義 | 1段階ブロックインデントを付ける |
| Block outdent | Ctrl+[ | 未定義 | 1段階ブロックインデントを取り除く |
| Undo | Ctrl+Z | Cmd+Z | 1ステップ戻す |
| Redo | Ctrl+Shift+Z, Ctrl+Y | Cmd+Shift+Z, Cmd+Y | 1ステップやり直す |
| Toggle comment | Ctrl+/ | Cmd+/ | コメントのトグル |
| Toggle block comment | Ctrl+Shift+/ | Cmd+Shift+/ | ブロックコメントのトグル |
| Transpose letters | Alt+Shift+X | Ctrl+T | カーソル位置の左の文字とカーソル位置の文字を入れ替える |
| Change to upper case | Ctrl+U | Ctrl+U | 大文字にする |
| Change to lower case | Ctrl+Shift+U, Alt+U | Ctrl+Shift+U | 小文字にする |
| Overwrite | Insert | Insert | 挿入と上書きの切り替え |
| Delete | Delete | Delete, Ctrl+D, Shift+Delete | デリート |
| Backspace | Shift+Backspace, Backspace | Ctrl+Backspace, Shift+Backspace, Backspace, Ctrl+H | バックスペース |
| Replay macro | Ctrl+Shift+E | Cmd+Shift+E | マクロの再生 |
| Toggle recording | Ctrl+Alt+E | Cmd+Opt+E | マクロの録画 |
| Center selection | 未定義 | Ctrl+L | 選択中あるいはカーソル位置を画面中央に移動 |
| Sort lines | Ctrl+Alt+S | Cmd+Opt+S | 選択中の複数の行を昇順に並べ替える |
| Open link | Ctrl+F3 | F3 | ブラウザでリンクを開く |
| Cut or delete | Shift+Delete | 未定義 | 削除 |
| Modify number up | Ctrl+Shift+Up | Opt+Shift+Up | カーソル位置の数値を1上げる |
| Modify number down | Ctrl+Shift+Down | Opt+Shift+Down | カーソル位置の数値を1下げる |

## キーボードショートカットの割り振られていない機能

| 動作 | 説明 |
|:-----------|:-----------|
| Add new line after the current line | 現在行の後に1行追加 |
| Add new line before the current line | 現在行の前に1行追加 |
| Auto Indent | 自動インデントのトグル |
| Change language mode | ハイライトモードの切替 |
| Fold to level 1~8 | レベルに応じて全体を畳む |
| Invert selection | 選択と非選択を入れ替える |
| Join lines | 選択中の全ての行を連結 |
| Pass keys to browser | キー入力をブラウザに送るモードに移行 |

## 設定

### Editor

| 名称 | 値 | 初期値 | 説明 |
|:-----------|:-----------|:-----------|:-----------|
| animatedScroll | Boolean | false | スクロールのアニメーション効果 |
| autoScrollEditorIntoView | Boolean | false |  |
| behavioursEnabled | Boolean | true |  |
| copyWithEmptySelection | Boolean | false |  |
| cursorStyle | String ("ace" \| "slim" \| "smooth" \| "smooth slim" \| "wide") | "ace" | カーソルの形状 |
| displayIndentGuides | Boolean | true |  |
| dragDelay | Int | 0 |  |
| dragEnabled | Boolean | true |  |
| enableAutoIndent | Boolean | true | 自動インデント有効化 |
| enableLinking | Boolean | true | リンク有効化 |
| enableMultiselect | Boolean | true | マルチセレクト有効化 |
| fadeFoldWidgets | Boolean | false | 折りたたみウィジェットを時間経過でフェードアウトする |
| fixedWidthGutter | Boolean | true |  |
| focusTimeout | Int | 0 |  |
| fontFamily | String | "monospace" | フォントファミリー |
| fontSize | Int | 14 | フォントの大きさ (ピクセル) |
| highlightActiveLine | Boolean | true | 現在のカーソル行を目立たせる |
| highlightGutterLine | Boolean | true |  |
| highlightIndentGuides | Boolean | true |  |
| highlightSelectedWord | Boolean | true | 選択中の単語を目立たせる |
| hScrollBarAlwaysVisible | Boolean | false | 垂直スクロールバー表示 |
| keyboardHandler | String \| null (別表参照) | null | キーボードショートカットの種類 |
| mergeUndoDeltas | String ("always" \| "false" \| "true") | "always" |  |
| navigateWithinSoftTabs | Boolean | false |  |
| printMarginColumn | Int | 80 | マージンの桁数 |
| readOnly | Boolean | false | 編集禁止 |
| scrollPastEnd | Int | 0 |  |
| scrollSpeed | Int | 2 |  |
| selectionStyle | String ("text" \| "line") | "line" | 行選択の表示形状 |
| showFoldWidgets | Boolean | true | 折りたたみウィジェットを表示する |
| showGutter | Boolean | true |  |
| showInvisibles | Boolean | false | タブや空白など非表示の文字を表示する |
| showLineNumbers | Boolean | true | 行数を表示する |
| showPrintMargin | Boolean | true | マージンの縦線を表示する |
| theme | String (別表参照) | "ace/theme/chrome" | アプリのテーマ (見た目) |
| tooltipFollowsMouse | Boolean | true |  |
| useTextareaForIME | Boolean | true |  |
| vScrollBarAlwaysVisible | Boolean | false | 水平スクロールバー表示 |
| wrapBehavioursEnabled | Boolean | true |  |

### Edit session

| 名称 | 値 | 初期値 | 説明 |
|:-----------|:-----------|:-----------|:-----------|
| firstLineNumber | Int | 1 | 最初の行番号 |
| foldStyle | String ("manual" \| "markbegin" \| "markbeginend") | "markbegin" | 折りたたみ表示スタイル |
| indentedSoftWrap | Boolean | true | 折り返し時にインデントする |
| mode | String (別表参照) | 未定義 | 構文ハイライト名 |
| newLineMode | String ("auto" \| "unix" \| "windows") | "auto" | 改行コード名 |
| overwrite | Boolean | false | 上書きモード有効化 |
| tabSize | Int | 4 | タブの文字数 |
| useSoftTabs | Boolean | true | ソフトタブ使用 |
| useWorker | Boolean | true |  |
| wrap | String ("off" \| "free" \| "printMargin" \| "40") \| Boolean | false | 折り返し処理 |

### Extensions

| 名称 | 値 | 初期値 | 説明 |
|:-----------|:-----------|:-----------|:-----------|
| enableBasicAutocompletion | Boolean | false | 基本オートコンプリート拡張機能の有効化 |
| enableCodeLens | Boolean | false |  |
| enableEmmet | Boolean | false | Emmet 拡張機能の有効化 |
| enableLiveAutocompletion | Boolean | false | ライブオートコンプリート拡張機能の有効化 |
| enableSnippets | Boolean | false | スニペットの有効化 |
| useElasticTabstops | Boolean | false | Elastic Tabstops 拡張機能使用 |

## 付録

### US 配列と JIS 配列の対応表

このリファレンスを読むにあたって必要最小限の対応をまとめている

| US 配列 | JIS 配列 |
|:-----------|:-----------|
| [ | @ |
| ] | [ |

