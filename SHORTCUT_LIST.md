# Keyboard shortcuts

[日本語版](https://gitlab.com/tatsuokitakayuki/tungsten/-/blob/master/SHORTCUT_LIST-JP.md)

## Line operations

| Action | Windows/Linux/ChromeOS | Mac |
|:-----------|:-----------|:-----------|
| Remove line | Ctrl+D | Cmd+D |
| Copy lines up | Alt+Shift+Up | Cmd+Opt+Up |
| Copy lines down | Alt+Shift+Down | Cmd+Opt+Down |
| Move lines up | Alt+Up | Opt+Up |
| Move lines down | Alt+Down | Opt+Down |
| Remove to line start | Alt+Backspace | Cmd+Backspace |
| Remove to line end | Alt+Delete | Ctrl+K, Cmd+Delete |
| Remove to line start hard | Ctrl+Shift+Backspace |  |  |
| Remove to line end hard | Ctrl+Shift+Delete |  |  |
| Remove word left | Ctrl+Backspace | Opt+Backspace, Ctrl+Opt+Backspace |
| Remove word right | Ctrl+Delete | Opt+Delete |
| Split line |  | Ctrl+O |

## Selection

| Action | Windows/Linux/ChromeOS | Mac |
|:-----------|:-----------|:-----------|
| Select all | Ctrl+A | Cmd+A |
| Select left | Shift+Left | Shift+Left, Ctrl+Shift+B |
| Select right | Shift+Right | Shift+Right |
| Select word left | Ctrl+Shift+Left | Opt+Shift+Left |
| Select word right | Ctrl+Shift+Right | Opt+Shift+Right |
| Select line start | Shift+Home | Shift+Home |
| Select line end | Shift+End | Shift+End |
| Select to line start | Alt+Shift+Left | Cmd+Shift+Left, Ctrl+Shift+A |
| Select to line end | Alt+Shift+Right | Cmd+Shift+Right, Ctrl+Shift+E |
| Select up | Shift+Up | Shift+Up, Ctrl+Shift+P |
| Select down | Shift+Down | Shift+Down, Ctrl+Shift+N |
| Select page up | Shift+PageUp | Shift+PageUp |
| Select page down | Shift+PageDown | Shift+PageDown |
| Select to start | Ctrl+Shift+Home | Cmd+Shift+Home, Cmd+Shift+Up |
| Select to end | Ctrl+Shift+End | Cmd+Shift+End, Cmd+Shift+Down |
| Duplicate selection | Ctrl+Shift+D | Cmd+Shift+D |
| Select to matching | Ctrl+Shift+\\, Ctrl+Shift+P | Cmd+Shift+\\ |
| Expand to matching | Ctrl+Shift+M | Ctrl+Shift+M |  |

## Multi-cursor

| Action | Windows/Linux/ChromeOS | Mac |
|:-----------|:-----------|:-----------|
| Add cursor above | Ctrl+Alt+Up | Ctrl+Opt+Up |
| Add cursor below | Ctrl+Alt+Down | Ctrl+Opt+Down |
| Add next occurrence to multi-selection | Ctrl+Alt+Right | Ctrl+Opt+Right |
| Add previous occurrence to multi-selection | Ctrl+Alt+Left | Ctrl+Opt+Left |
| Move multi-cursor from current line to the line above | Ctrl+Alt+Shift+Up | Ctrl+Opt+Shift+Up |
| Move multi-cursor from current line to the line below | Ctrl+Alt+Shift+Down | Ctrl+Opt+Shift+Down |
| Remove current occurrence from multi-selection and move to next | Ctrl+Alt+Shift+Right | Ctrl+Opt+Shift+Right |
| Remove current occurrence from multi-selection and move to previous | Ctrl+Alt+Shift+Left | Ctrl+Opt+Shift+Left |
| Select all from multi-selection (Expand to line) | Ctrl+Shift+L | Ctrl+Shift+L |
| Align cursors | Ctrl+Alt+A | Ctrl+Opt+A |

## Go to

| Action | Windows/Linux/ChromeOS | Mac |
|:-----------|:-----------|:-----------|
| Go to left | Left | Left, Ctrl+B |
| Go to right | Right | Right, Ctrl+F |
| Go to word left | Ctrl+Left | Opt+Left |
| Go to word right | Ctrl+Right | Opt+Right |
| Go line up | Up | Up, Ctrl+P |
| Go line down | Down | Down, Ctrl+N |
| Go to line start | Alt+Left, Home | Cmd+Left, Home, Ctrl+A |
| Go to line end | Alt+Right, End | Cmd+Right, End, Ctrl+E |
| Go to page up | PageUp | PageUp |
| Go to page down | PageDown | PageDown, Ctrl+V |
| Go to start | Ctrl+Home | Cmd+Home, Cmd+Up |
| Go to end | Ctrl+End | Cmd+End, Cmd+Down |
| Go to line | Ctrl+L | Cmd+L |
| Scroll up | Ctrl+Up |  |
| Scroll down | Ctrl+Down |  |
| Page up |  | Opt+PageUp |
| Page down |  | Opt+PageDown |
| Jump to matching | Ctrl+\\, Ctrl+P | Cmd+\\ |
| Go to next error | Alt+E | F4 |
| Go to previous error | Alt+Shift+E | Shift+F4 |

## Find/Replace

| Action | Windows/Linux/ChromeOS | Mac |
|:-----------|:-----------|:-----------|
| Find | Ctrl+F | Cmd+F |
| Replace | Ctrl+H | Cmd+Opt+F |
| Find next | Ctrl+K | Cmd+G |
| Find Previous | Ctrl+Shift+K | Cmd+Shift+G |
| Find all | Ctrl+Alt+K |  |
| Select or find next | Alt+K | Ctrl+G |
| Select or find previous | Alt+Shift+K | Ctrl+Shift+G |

## Folding

| Action | Windows/Linux/ChromeOS | Mac |
|:-----------|:-----------|:-----------|
| Fold selection | Alt+L, Ctrl+F1 | Cmd+Opt+L, Cmd+F1 |
| Unfold | Alt+Shift+L, Ctrl+Shift+F1 | Cmd+Opt+Shift+L, Cmd+Shift+F1 |
| Fold all |  | Ctrl+Cmd+Opt+0 |
| Fold other | Alt+0 | Cmd+Opt+0 |
| Unfold all | Alt+Shift+0 | Cmd+Opt+Shift+0 |
| Fold all comments |  | Ctrl+Cmd+Opt+0 |
| Toggle fold widget | F2 | F2 |
| Toggle parent fold widget | Alt+F2 | Opt+F2 |

## Help

| Action | Windows/Linux/ChromeOS | Mac |
|:-----------|:-----------|:-----------|
| Command palette | F1 |  |
| Show settings menu | Ctrl+, | Cmd+, |
| Show keyboard shortcuts | Ctrl+Alt+H | Cmd+Opt+H |

## File

| Action | Windows/Linux/ChromeOS | Mac |
|:-----------|:-----------|:-----------|
| New file | Ctrl+N | Cmd+N |
| Open file | Ctrl+O | Cmd+O |
| Save file | Ctrl+S | Cmd+S |
| Rename file | Alt+R | Opt+R |
| Close file | Alt+W | Opt+W |
| Quit app | Ctrl+W | Cmd+W |

## File list

| Action | Windows/Linux/ChromeOS | Mac |
|:-----------|:-----------|:-----------|
| Toggle file list | Ctrl+Shift+F |  |
| Next file | Ctrl+Tab |  |
| Previous file | Ctrl+Shift+Tab |  |
| Select 1st to 8th file | Ctrl+1~8 |  |
| Select first file | Ctrl+0 |  |
| Select last file | Ctrl+9 |  |

## Other

| Action | Windows/Linux/ChromeOS | Mac |
|:-----------|:-----------|:-----------|
| Cut | Ctrl+X | Cmd+X |
| Copy | Ctrl+C | Cmd+C |
| Paste | Ctrl+V | Cmd+V |
| Indent | Tab | Tab |
| Outdent | Shift+Tab | Shift+Tab |
| Block indent | Ctrl+] |  |
| Block outdent | Ctrl+[ |  |
| Undo | Ctrl+Z | Cmd+Z |
| Redo | Ctrl+Shift+Z, Ctrl+Y | Cmd+Shift+Z, Cmd+Y |
| Toggle comment | Ctrl+/ | Cmd+/ |
| Toggle block comment | Ctrl+Shift+/ | Cmd+Shift+/ |
| Transpose letters | Alt+Shift+X | Ctrl+T |
| Change to upper case | Ctrl+U | Ctrl+U |
| Change to lower case | Ctrl+Shift+U, **Alt+U** | Ctrl+Shift+U |
| Overwrite | Insert | Insert |
| Delete | Delete | Delete, Ctrl+D, Shift+Delete |
| Backspace | Shift+Backspace, Backspace | Ctrl+Backspace, Shift+Backspace, Backspace, Ctrl+H |
| Replay macro | Ctrl+Shift+E | Cmd+Shift+E |
| Toggle recording | Ctrl+Alt+E | Cmd+Opt+E |
| Center selection |  | Ctrl+L |
| Sort lines | Ctrl+Alt+S | Cmd+Opt+S |
| Open link | Ctrl+F3 | F3 |
| Cut or delete | Shift+Delete |  |
| Modify number up | Ctrl+Shift+Up | Opt+Shift+Up |
| Modify number down | Ctrl+Shift+Down | Opt+Shift+Down |
