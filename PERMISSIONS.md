# About permissions

[Japanese version](https://gitlab.com/tatsuokitakayuki/tungsten/-/blob/master/PERMISSIONS-JP.md)

## The permissions and requirements that the application requires from the user

1. File editing
  * Requests "read permission" for the target file when opening the file.
  * Requests "read / write permission" for the target file when saving the file.

## Procedure for revoking permissions

1. File editing
  * You can revoke the permission by selecting the document icon (📄) on the far left of the icon list on the right side of the window title bar and selecting the "Remove Access" button.

