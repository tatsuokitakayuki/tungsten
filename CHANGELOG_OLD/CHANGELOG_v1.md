# CHANGELOG

## Version 1.0.20200207 (dev)
- Fixed onClosed function.

## Version 1.0.20200206 (dev)
- Fixed dialog action.
- Added URL to file list item.
- Fixed File data URL.

## Version 1.0.20200205 (dev)
- Fixed the behavior of Dialog class.
- Improved removeChildren behavior.
- Remove unused methods.

## Version 1.0.20200203 (dev)
- Fixed Cancel button.
- Refactored Dialog class and Material Helper class.

## Version 1.0.20200131 (stable)
- Updated for stable (version 1, maintenance 2) release.
