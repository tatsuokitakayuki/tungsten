# CHANGELOG

## Version 3.2.20200729 (dev)
- Improved focus movement when canceling Import options only by mouse operation.
- Improved focus movement when canceling Open file only by mouse operation.

## Version 3.2.20200727 (dev)
- Improved the definition of non-selectable parts.

## Version 3.2.20200726 (dev)
- Adjusted the contents of the status of all parts.
- Improved the process of adding all commands of OED.

## Version 3.2.20200724 (dev)
- Adjusted some background colors.

## Version 3.2.20200723 (dev)
- Adjusted some background colors.
- Improved the key input processing of the list.

## Version 3.2.20200723 (dev)
- Updated Material Components for the web to version 7.0.0.
- Updated localForage to version 1.8.1.

## Version 3.2.20200721 (dev)
- Formatted source code.
- Updated localForage to version 1.8.0.

## Version 3.2.20200720 (dev)
- Fixed to focus on EditField after closing the dialog.
- Fixed a bug that crashes when updating the status.
- Fixed to update the status after selecting a menu item.
- Fixed the behavior of the menu.

## Version 3.2.20200712 (dev)
- Refactored Drawer class.

## Version 3.2.20200710 (dev)
- Fixed focus getting in editor.

## Version 3.2.20200709 (dev)
- Fixed all item names to refer to res.js.

## Version 3.2.20200706 (dev)
- Updated ace-builds to version 1.4.12.
- Fixed OPEN_SOURCE_LICENSE.md.

## Version 3.2.20200706 (dev)
- Fixed some display not selectable.

## Version 3.2.20200703 (dev)
- Fixed some display not selectable.
- Fixed the contents of Statusbar.
- Fixed id.

## Version 3.2.20200702 (dev)
- Added Statusbar.

## Version 3.2.20200630 (dev)
- Fixed to get editor with getter.

## Version 3.1.20200626 (dev)
- Fixed to update the display when the app gets focus.

## Version 3.1.20200623 (dev)
- Fixed a bug that it may stop at Close file processing.
- Fixed CSS.

## Version 3.1.20200623 (dev)
- Restored the behavior of the dialog buttons.
- Fixed CSS.

## Version 3.1.20200622 (dev)
- Fixed the behavior of dialog buttons.
- Fixed CSS.

## Version 3.1.20200619 (dev)
- Fixed status message.

## Version 3.1.20200618 (dev)
- Modified the definition of EventListener.

## Version 3.1.20200615 (dev)
- Adjusted the display of File list.

## Version 3.1.20200612 (dev)
- Fixed CSS.

## Version 3.1.20200611 (dev)
- Fixed Ace Dark/Light settings to be applied to OED display.
- Extracted the definition of Theme from Res class to ThemeHelper class.
- Fixed the argument of ChangeViewEvent.
- Removed some unnecessary imports.
- Fixed CSS.

## Version 3.1.20200610 (dev)
- Fixed initial value of Font family to "monospace".

## Version 3.1.20200609 (dev)
- Added Go to line.
- Fixed CSS.

## Version 3.1.20200608 (dev)
- Added dark mode and light mode.
- Updated localForage to version 1.7.4.

## Version 3.1.20200605 (dev)
- Fixed to save options with Import options.

## Version 3.1.20200604 (dev)
- Added Snackbar to Export options and Import options.

## Version 3.1.20200601 (dev)
- Fixed the Snackbar.
- Arranged the order of arguments.

## Version 3.0.20200526 (dev)
- Added Open source license to the help menu.

## Version 3.0.20200522 (dev)
- Fixed some strings.
- Fixed the color of the close button on Snackbar.

## Version 3.0.20200520 (dev)
- Fixed options for event listener.
- Fixed the value of data-mdc-dialog-button-default.

## Version 3.0.20200515 (dev)
- Added OED update notification.

## Version 3.0.20200511 (dev)
- Changed Toggle file list command to ToggleDrawerEvent class.
- Added ToggleDrawerEvent class.

## Version 3.0.20200508 (dev)
- Cleaned up some elements.
- Fixed initial data of AppBar.
- Implemented the function of Menu button option.

## Version 3.0.20200507 (dev)
- Added Menu button option.

## Version 3.0.20200506 (dev)
- Fixed a subtle shade of app icons.
- Fixed the initial value.

## Version 3.0.20200505 (dev)
- Fixed a mistake in color name.

## Version 3.0.20200504 (dev)
- Changed the design of the app icon.
- Added options found in the kitchen sink demo.

## Version 3.0.20200502 (dev)
- Fixed app icon settings.

## Version 3.0.20200501 (dev)
- Renamed the class name from UIHelper to UiHelper.
- Cleaned up some code.
