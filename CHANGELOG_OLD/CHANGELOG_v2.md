# CHANGELOG

## Version 2.0.20200430 (stable)
- Updated for stable (version 2, maintenance 2) release.

## Version 2.0.20200424 (dev)
- Fixed lang attribute of index.html.

## Version 2.0.20200424 (dev)
- Improved the behavior of Rename file.
- Replaced some import modules.

## Version 2.0.20200423 (dev)
- Moved Read only from View menu to File menu.
- Fixed some View menu items.
- Fixed the behavior when closing the dialog.
- Fixed saving options.
- Added two events to save options.

## Version 2.0.20200422 (dev)
- Fixed the argument of each dialog.
- Fixed the Cursor Style option.
- Removed some unnecessary imports.
- Fixed Snackbar update event handling.

## Version 2.0.20200421 (dev)
- Replaced element i with span.
- Cleaned up MaterialHelper class and Drawer class.

## Version 2.0.20200420 (dev)
- Added a close action to Snackbar.

## Version 2.0.20200417 (dev)
- Added options.
- Renamed "keybinding" option to "keyboardHandler".
- Fixed a bug of Rename file called from Download file.

## Version 2.0.20200416 (dev)
- Cleaned up all title strings.

## Version 2.0.20200415 (dev)
- Fixed the menu name to refer to res.js.
- Fixed the option name same as Ace Editor.
- Fixed a bug that Theme dialog doesn't open.
- Updated ace-builds to version 1.4.10.

## Version 2.0.20200414 (dev)
- Deleted unnecessary contents.
- Updated the title string.
- Fixed spelling 'keybindings' to 'keybinding'.
- Moved description string literal from keybinding.js to res.js.

## Version 2.0.20200413 (dev)
- Assigned "Select last file" to Ctrl+9.
- Assigned "Select 1st to 8th file" to Ctrl+1~8.
- Improved Select file.

## Version 2.0.20200412 (dev)
- Fixed some elements.

## Version 2.0.20200410 (dev)
- Fixed initialization of all menu items.
- Improved to confirm whether to close the app only when there is a changed file.

## Version 2.0.20200409 (dev)
- Improved setOption.
- Improved menu updating.
- Renamed "Mode" to "Language mode".
- Updated ignore list in File menu.
- Added description to all added commands.
- Added Sort lines to Edit menu.

## Version 2.0.20200408 (dev)
- Added Save edit session options to File menu.
- Moved some View menu items to File menu.
- Updated ACE_URLS in sw.js.
- Updated ace-builds to version 1.4.9.
- Improved Close file.

## Version 2.0.20200407 (dev)
- Added ChangeEditorOptionEvent class.
- Changed class name from View to AppView.
- Improved import of OED options.
- Improved initialization of OED options.
- Removed unnecessary imports.

## Version 2.0.20200406 (dev)
- Added File decoding option.

## Version 2.0.20200403 (dev)
- Unnecessary rows were deleted.

## Version 2.0.20200403 (stable)
- Updated for stable (version 2, maintenance 1) release.

## Version 2.0.20200402 (stable)
- Updated ABOUT.md.

## Version 2.0.20200401 (stable)
- Initial release.

## Version 2.0.30200331 (dev)
- Updated Icon font loading settings.
- Fixed a bug in the Keybinding dialog.

## Version 2.0.30200330 (dev)
- Improved to automatically scroll when updating the file list.
- Improved loading of JavaScript files.
- Removed unnecessary attribute.

## Version 2.0.30200327 (dev)
- Fixed options when adding event listeners.
- Improved the contents of View menu.
- Improved to automatically scroll when updating the file list.

## Version 2.0.30200326 (dev)
- Fixed New file.

## Version 2.0.30200325 (dev)
- Added New line mode dialog.
- Added Fold style dialog.
- Modified to not set the mode when updating the screen.
- Added Mode dialog.
- Fixed Merge undo deltas bug.
- Removed unnecessary methods.
- Updated to extend DialogSelect class.
- Added DialogSelect class.

## Version 2.0.30200324 (dev)
- Renamed some methods to fix bugs.
- Prepared if statement.
- Fixed Export options.

## Version 2.0.30200323 (dev)
- Updated scrollPastEnd command definition.
- Renamed some methods.
- Organized menu items.
- Added First line number dialog.
- Fixed typo.
- Added attribute to file list in drawer.
- Updated Rename file.

## Version 2.0.20200320 (dev)
- Fixed the link between Download file and Rename file.
- Fixed to display Snackbar in Download file.
- Added Snackbar class.

## Version 2.0.20200319 (dev)
- Updated theme list.
- Fixed the definition of EditDocument.

## Version 2.0.20200317 (dev)
- Fixed app bar update.
- Fixed drawer item update.
- Fixed screen update.

## Version 2.0.20200316 (dev)
- Fixed screen update.
- Split View related methods from Core class.
- Fixed Rename file dialog.

## Version 2.0.20200312 (dev)
- Fixed many EventListener options.
- Added processing to resize the editor after opening and closing the drawer.

## Version 2.0.20200310 (dev)
- Improved all buttons.
- Updated Tab size dialog.
- Updated Print margin column dialog.
- Updated Font family dialog.
- Updated Font size dialog.
- Updated Rename file dialog.

## Version 2.0.20200306 (dev)
- Updated Prompt dialog.
- Updated Confirm dialog.
- Removed unnecessary code.

## Version 2.0.20200305 (dev)
- Added Snackbar.
- Added Wrap dialog.
- Improved option strings.

## Version 2.0.20200304 (dev)
- Added Import options.
- Removed unnecessary options.
- Added Export options.

## Version 2.0.20200303 (dev)
- Adjusted the width of select element.
- Fixed emmet-core management.
- Updated Material Design Components for Web to Version 5.1.0.

## Version 2.0.20200302 (dev)
- Fixed buildItem method of the Drawer class.
- Fixed the appearance of the File list.

## Version 2.0.20200228 (dev)
- Updated the display of shortcuts.

## Version 2.0.20200228 (stable)
- Updated for stable (version 2) release.

## Version 2.0.20200227 (dev)
- Updated the display of shortcuts.
- Renamed some methods of the FileData class.
- Fixed Rename file.

## Version 2.0.20200226 (dev)
- Fixed idToName method of the Core class.
- Sorted imports.
- Renamed some methods of the FileHelper class.
- Updated ABOUT.
- Updated README.
- Renamed some methods of the Core class.

## Version 2.0.20200225 (dev)
- Updated theme color.
- Changed some keybindings.
- Refactored some methods of the Core class.
- Refactored some methods of the FileData class.
- Added reload confirmation code.

## Version 2.0.20200224 (dev)
- Added tab size option dialog.
- Fixed initialize method of Core class.
- Renamed some methods of the FileManager class.
- Updated the contents of the window title.
- Renamed some methods of the Core class.
- Renamed FileDataList to FileManager.
- Renamed session to editSession.

## Version 2.0.20200221 (dev)
- Changed the contents of the window title.
- Removed unnecessary code.

## Version 2.0.20200220 (dev)
- Updated Help menu items.
- Updated View menu items.
- Added many option dialogs.

## Version 2.0.20200218 (dev)
- Added Cursor style dialog.

## Version 2.0.20200217 (dev)
- Changed Keybinding and Theme dialog styles back to browser native.
- Renamed some methods.
- Fixed read only.
- Fixed some methods.

## Version 2.0.20200214 (dev)
- Changed Theme dialog style.
- Refactored Keybinding dialog class.
- Changed Keybinding dialog style.

## Version 2.0.20200213 (dev)
- Updated cache list.
- Fixed 'change' event trigger.

## Version 2.0.20200212 (dev)
- Changed edit session management.
- Improved the display of '(Modified)'.

## Version 2.0.20200211 (dev)
- Improved FileList behavior.
