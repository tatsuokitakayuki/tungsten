#!/usr/bin/env python3

from pathlib import Path

app = Path.cwd()
app_public = app / 'public'
app_dialog = app_public / 'dialog'
app_res = app_public / 'res'
app_icons = app_public / 'images/icons'
app_screenshots = app_public / 'images/screenshots'

items = []
for x in app_public.iterdir():
  if x.is_file():
    items.append('' + x.name)
for x in app_dialog.iterdir():
  if x.is_file():
    items.append('dialog/' + x.name)
for x in app_res.iterdir():
  if x.is_file():
    items.append('res/' + x.name)
for x in app_icons.iterdir():
  if x.is_file():
    items.append('images/icons/' + x.name)
for x in app_screenshots.iterdir():
  if x.is_file():
    items.append('images/screenshots/' + x.name)
items.sort()
file = open('./app_list.txt', 'w');
for x in items:
  file.write('\'' + x + '\',\n')
file.close()
