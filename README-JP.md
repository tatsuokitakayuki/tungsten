# <🖊> Tungsten Text Editor

[English version](https://gitlab.com/tatsuokitakayuki/tungsten/-/blob/master/README.md)

[![Screenshot](https://gitlab.com/tatsuokitakayuki/tungsten/-/raw/master/public/images/screenshots/0000.jpg)](https://gitlab.com/tatsuokitakayuki/tungsten/-/blob/master/public/images/screenshots/0000.jpg)

[![Tungsten Text Editor | Product Hunt](https://api.producthunt.com/widgets/embed-image/v1/featured.svg?post_id=359946&theme=light)](https://www.producthunt.com/posts/tungsten-text-editor?utm_source=badge-featured&utm_medium=badge&utm_souce=badge-tungsten&#0045;text&#0045;editor)

## これはなにか？

Tungsten Text Editor はメモ帳 (Notepad) に似た小型軽量のテキスト編集アプリです。

主に Chromebook（あるいは Chromium OS やそれを元とした OS の）ユーザーのために Tungsten Text Editor を開発しています。

なお、[Web Capabilities (Project Fugu 🐡)](https://www.chromium.org/teams/web-capabilities-fugu/) の成果を利用してローカルファイルアクセス機能など一般的なウェブアプリでは実現不可能な機能を実装しています。

## 今すぐ試してみて！

- [安定版 (stable channel)](https://pwatungsten.app/)
- [開発版 (dev channel) ※不安定です！](https://dev.tungsten.app/)

配信にあたり [Firebase](https://firebase.google.com/) のホスティングサービスを使っています。

- [Google Play ストア版 (stable)](https://play.google.com/store/apps/details?id=app.kirari.tungsten.twa)

Google Play ストアで安定版のアプリパッケージを配信しています。

## ローカルで試す方法

1. このリポジトリを clone します。
  - `git clone https://gitlab.com/tatsuokitakayuki/tungsten.git`
2. public ディレクトリに移動します。
  - `cd tungsten/public/`
3. public ディレクトリの中に多数のディレクトリとファイルを追加します。
  - public/sw.js にファイル一覧があります。
  - そのうちセットアップスクリプトを作ります。
4. HTTP サーバーを起動します。
  - `python3 -m http.server --bind 127.0.0.1 8080`
5. ウェブブラウザで localhost:8080 を開きます。

ぜひお試しください。

## システム要件

### OS

- ChromeOS（重要対象、動作確認済み）
- Linux（動作確認報告あり）
- Windows（動作確認済み）
- macOS
- Android（動作確認済み）
- iOS
- iPadOS

### ブラウザ

- Chrome（バージョン 63 以上、重要対象、動作確認済み）
- Edge（バージョン 79 以上）
- Firefox（バージョン 67 以上）
- Safari（バージョン 11.1 以上）

## Tungsten で使用しているライブラリやフレームワークの一覧

* [Ace](https://ace.c9.io/)
* [Ace Editor Extensions for Tungsten Text Editor](https://gitlab.com/tatsuokitakayuki/ace_ext_tungsten)
* [Material Design Component for Web](https://material.io/develop/web/)
* [Material Design Icons](https://developers.google.com/fonts/docs/material_icons)
* [localForage](https://localforage.github.io/localForage/)
* [emmet-core](https://github.com/cloud9ide/emmet-core)

## ライセンス

このプロジェクトはオープンソースであり、[MIT License](https://gitlab.com/tatsuokitakayuki/tungsten/-/blob/master/LICENSE) の下で利用可能です。

## このプロジェクトをサポートしてください！🙏

* [Support on Patreon](https://www.patreon.com/TatsuokiTakayuki)
* [Support on pixivFANBOX](https://tatsuokitakayuki.fanbox.cc/)
* [Product sales site: きらりスターどっとこむ物品販売部](https://kiraristar.booth.pm/)

## Authors

* メンテナ, 開発者: 龍興 尚幸 (TATSUOKI Takayuki)
  - [𝕏: @ttatsuoki](https://twitter.com/ttatsuoki)
  - [Email: takayuki.tatsuoki@gmail.com](mailto:takayuki.tatsuoki@gmail.com)
