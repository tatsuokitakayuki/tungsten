# <🖊> Tungsten Text Editor

[日本語版](https://gitlab.com/tatsuokitakayuki/tungsten/-/blob/master/README-JP.md)

[![Screenshot](https://gitlab.com/tatsuokitakayuki/tungsten/-/raw/master/public/images/screenshots/0000.jpg)](https://gitlab.com/tatsuokitakayuki/tungsten/-/blob/master/public/images/screenshots/0000.jpg)

[![Tungsten Text Editor | Product Hunt](https://api.producthunt.com/widgets/embed-image/v1/featured.svg?post_id=359946&theme=light)](https://www.producthunt.com/posts/tungsten-text-editor?utm_source=badge-featured&utm_medium=badge&utm_souce=badge-tungsten&#0045;text&#0045;editor)

## What is this?

Tungsten Text Editor is a small, lightweight notepad-like text editor.

We are developing Tungsten Text Editor mainly for Chromebook (or Chromium OS and its based OS) users.

In addition, the results of [Web Capabilities (Project Fugu 🐡)](https://www.chromium.org/teams/web-capabilities-fugu/) are used to implement features that are not feasible for general web applications, such as local file access functions.

## Try it now!

- [stable channel](https://pwatungsten.app/)
- [dev channer (UNSTABLE!)](https://dev.pwatungsten.app/)

We use [Firebase](https://firebase.google.com/)'s hosting service for delivery.

- [Google Play Store (stable)](https://play.google.com/store/apps/details?id=app.kirari.tungsten.twa)

A stable version of the application package is available from the Google Play Store.

## How to try locally

1. Clone this repository.
  - `git clone https://gitlab.com/tatsuokitakayuki/tungsten.git`
2. Change to the public directory.
  - `cd tungsten/public/`
3. Add directories and files in the public directory.
  - You can find a list of files in public/sw.js.
  - We'll make a setup script soon.
4. Start the HTTP server.
  - `python3 -m http.server --bind 127.0.0.1 8080`
5. Open localhost:8080 in your web browser.

Please try.

## System requirements

### OS

- ChromeOS (Main target, Tested)
- Linux (Reported to work)
- Windows (Tested)
- macOS
- Android (Tested, but unstable)
- iOS
- iPadOS

### Browser

- Chrome (Version 63 or higher, Main target, Tested)
- Edge (Version 79 or higher)
- Firefox (Version 67 or higher)
- Safari (Version 11.1 or higher)

## List of libraries and frameworks used

* [Ace](https://ace.c9.io/)
* [Ace Editor Extensions for Tungsten Text Editor](https://gitlab.com/tatsuokitakayuki/ace_ext_tungsten)
* [Material Design Component for Web](https://material.io/develop/web/)
* [Material Design Icons](https://developers.google.com/fonts/docs/material_icons)
* [localForage](https://localforage.github.io/localForage/)
* [emmet-core](https://github.com/cloud9ide/emmet-core)

## License

This project is open source and available under the [MIT License](https://gitlab.com/tatsuokitakayuki/tungsten/-/blob/master/LICENSE).

## Please support this project!🙏

* [Support on Patreon](https://www.patreon.com/TatsuokiTakayuki)
* [Support on pixivFANBOX](https://tatsuokitakayuki.fanbox.cc/)
* [Product sales site: Kiraristar.com Product Sales Department](https://kiraristar.booth.pm/)

## Authors

* Maintainer, Developer: 龍興 尚幸 (TATSUOKI Takayuki)
  - 𝕏: [@ttatsuoki](https://twitter.com/ttatsuoki)
  - Email: [takayuki.tatsuoki@gmail.com](mailto:takayuki.tatsuoki@gmail.com)
