import {HtmlHelper} from '/html_helper.js';
import {StatusHelper} from '/status_helper.js';

let NEW_LINE_MODE = null;

export class Statusbar {
  initialize(resources) {
    NEW_LINE_MODE = resources.NEW_LINE_MODE;
    this.htmlHelper = HtmlHelper.newHelper();
    this.element = this.htmlHelper.elementById('statusbar');
    if (this.element) {
      this.htmlHelper.listen(
        'Statusbar:change',
        event => this.onChange(event),
        {passive: true},
        document
      );
    }
  }

  onChange(event) {
    const {name, editor, fileData} = event.detail;
    const statusHelper = new StatusHelper({NEW_LINE_MODE: NEW_LINE_MODE});
    const items = statusHelper.buildStatusText(name, editor, fileData);
    this.htmlHelper.removeChildren(this.element);
    for (const item of items) {
      this.element.appendChild(item);
    }
  }
}
