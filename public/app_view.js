import {ChangeDrawerItemEvent} from '/change_drawer_item_event.js';
import {ChangeStatusbarEvent} from '/change_statusbar_event.js';
import {HtmlHelper} from '/html_helper.js';
import {ThemeHelper} from '/theme_helper.js';

export class AppView {
  constructor(core) {
    this.core = core;
  }

  initialize() {
    this.htmlHelper = HtmlHelper.newHelper();
    this.htmlHelper.listen(
      'View:change', event => this.onChange(event), {passive: true}, document
    );
  }

  onChange(event) {
    if (event) {
      const {fromTo, flags} = event.detail;
      const {from, to} = fromTo;
      this.update(from, to, flags);
    }
  }

  getEditor() {
    return this.core.getEditor();
  }

  getDisplayName(index) {
    return this.core.getDisplayName(index);
  }

  getEditSession(index) {
    return this.core.getEditSession(index);
  }

  getFileData(index) {
    return this.core.getFileData(index);
  }

  getLength() {
    return this.core.getLength();
  }

  getOption(name) {
    return this.core.getOption(name);
  }

  isReadOnly(index) {
    return this.core.isReadOnly(index);
  }

  update(from, to, flags) {
    const editor = this.getEditor();
    if (flags.all || flags.editor) {
      if (Number.isInteger(to) && to >= 0 && to < this.getLength()) {
        this.updateEditor(editor, to);
      }
    }
    if (flags.all || flags.draweritem) {
      if (Number.isInteger(from) && from >= 0 && from < this.getLength() &&
          Number.isInteger(to) && to >= 0 && to < this.getLength()) {
        document.dispatchEvent(
          new ChangeDrawerItemEvent(from, to)
        );
      }
    }
    if (flags.all || flags.theme) {
      this.updateTheme();
    }
    if (flags.all || flags.statusbar) {
      if (Number.isInteger(to) && to >= 0 && to < this.getLength()) {
        document.dispatchEvent(
          new ChangeStatusbarEvent(
            this.getDisplayName(to),
            editor,
            this.getFileData(to)
          )
        );
      }
    }
  }

  updateEditor(editor, to) {
    editor.setSession(this.getEditSession(to));
    editor.setReadOnly(this.isReadOnly(to));
  }

  updateTheme() {
    const theme = this.getOption('theme');
    if (theme) {
      const target = this.htmlHelper.elementById('app-view');
      target.classList.remove(...['app-dark', 'app-light']);
      const themeHelper = new ThemeHelper();
      if (themeHelper.isDark(theme)) {
        target.classList.add('app-dark');
      } else {
        target.classList.add('app-light');
      }
    }
  }
}
