export class FocusEditorEvent extends CustomEvent {
  constructor() {
    super('Core:focuseditor');
  }
}
