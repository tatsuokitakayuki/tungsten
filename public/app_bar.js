import {ChangeDrawerEvent} from '/change_drawer_event.js';
import {HtmlHelper} from '/html_helper.js';
import {OpenCommandPaletteEvent} from '/open_command_palette_event.js';
import {SaveFileEvent} from '/save_file_event.js';

const MDCTopAppBar = mdc.topAppBar.MDCTopAppBar;

export class AppBar {
  initialize() {
    const htmlHelper = HtmlHelper.newHelper();
    const topAppBar = new MDCTopAppBar(htmlHelper.elementById('app-bar'));
    topAppBar.setScrollTarget(htmlHelper.elementById('editor-container'));
    topAppBar.listen(
      'MDCTopAppBar:nav',
      () => document.dispatchEvent(new ChangeDrawerEvent()),
      {passive: true}
    );
    htmlHelper.listen(
      'click',
      () => document.dispatchEvent(new SaveFileEvent()),
      {passive: true},
      htmlHelper.elementById('save-file-button')
    );
    htmlHelper.listen(
      'click',
      () => document.dispatchEvent(new OpenCommandPaletteEvent()),
      {passive: true},
      htmlHelper.elementById('command-palette-button')
    );
  }
}
