export class OpenCommandPaletteEvent extends CustomEvent {
  constructor() {
    super('Editor:commandpalette');
  }
}
