const EditDocument = ace.require('ace/document').Document;
const EditSession = ace.EditSession;
const UndoManager = ace.UndoManager;

let FILE_URL = null;
let FILES = null;

export class FileData {
  constructor(resources) {
    FILE_URL = resources.FILE_URL;
    FILES = resources.FILES;
    this.setEditSession(new EditSession(new EditDocument('')));
    this.setUndoManager(new UndoManager());
    this.setName('');
    this.setDisplayName('');
    this.setUrl(new URL(
      FILE_URL.PROTOCOL + '//' +
      location.host + '/' +
      FILE_URL.DIRS.INTERNAL + '/' + ''
    ));
    this.setType('text/plain');
    this.setReadOnly(false);
    this.setTime(new Date().getTime());
    this.setFileHandle(null);
  }

  getDisplayName() {
    return this.displayName;
  }

  getEditSession() {
    return this.editSession;
  }

  getFileHandle() {
    return this.fileHandle;
  }

  getMode() {
    return this.getEditSession().getMode();
  }

  getName() {
    return this.name;
  }

  getTime() {
    return this.time;
  }

  getType() {
    return this.type;
  }

  getUndoManager() {
    return this.getEditSession().getUndoManager();
  }

  getUrl() {
    return this.url;
  }

  getValue() {
    return this.getEditSession().getValue();
  }

  hasRedo() {
    return this.getUndoManager().hasRedo();
  }

  hasUndo() {
    return this.getUndoManager().hasUndo();
  }

  isClean() {
    return this.getUndoManager().isClean();
  }

  isCoreFile() {
    if (this.getUrl().pathname.includes('/' + FILE_URL.DIRS.INTERNAL + '/')) {
      return Boolean(
        Object.values(FILES).find(
          name => this.getUrl().pathname.includes(name)
        )
      );
    }
    return false;
  }

  isEmptyFile() {
    if (this.getValue()) {
      return false;
    }
    if (!this.isClean()) {
      return false;
    }
    return this.getUrl().pathname.includes('/' + FILE_URL.DIRS.INTERNAL + '/');
  }

  isReadOnly() {
    return this.readOnly;
  }

  markClean() {
    this.getUndoManager().markClean();
  }

  setDisplayName(displayName) {
    this.displayName = displayName;
  }

  setEditSession(editSession) {
    this.editSession = editSession;
  }

  setFileHandle(fileHandle) {
    this.fileHandle = fileHandle;
  }

  setMode(mode) {
    this.getEditSession().setMode(mode);
  }

  setName(name) {
    this.name = name;
  }

  setReadOnly(readOnly) {
    this.readOnly = readOnly;
  }

  setTime(time) {
    this.time = time;
  }

  setType(type) {
    this.type = type;
  }

  setUndoManager(undoManager) {
    this.getEditSession().setUndoManager(undoManager);
  }

  setUrl(url) {
    this.url = url;
  }

  setValue(text) {
    this.getEditSession().setValue(text);
  }
}
