import {CacheManager} from '/cache_manager.js';
import {FileData} from '/file_data.js';
import {FileHelper} from '/file_helper.js';
import {HtmlHelper} from '/html_helper.js';

let FILE_URL = null;
let FILES = null;
let STRINGS = null;

export class FileManager {
  constructor() {
    this.list = [];
    this.active = -1;
    this.untitledCount = 0;
    this.fileHelper = FileHelper.newHelper();
  }

  initialize(resources) {
    FILE_URL = resources.FILE_URL;
    FILES = resources.FILES;
    STRINGS = resources.STRINGS;
  }

  addEventListener() {
    const htmlHelper = HtmlHelper.newHelper();
    htmlHelper.listen(
      'File:rename',
      event => this.onRenameFile(event),
      {passive: true, once: true},
      document
    );
  }

  onRenameFile(event) {
    const {name, callback, args} = event.detail;
    if (name) {
      this.renameFile(this.getActive(), name);
      if (callback) {
        callback(args);
      }
    }
  }

  getActive() {
    return this.active;
  }

  getDisplayName(index) {
    return this.list[index].getDisplayName();
  }

  getEditSession(index) {
    return this.list[index].getEditSession();
  }

  getFileData(index) {
    return this.list[index];
  }

  getFileHandle(index) {
    return this.list[index].getFileHandle();
  }

  getName(index) {
    return this.list[index].getName();
  }

  getTime(index) {
    return this.list[index].getTime();
  }

  getType(index) {
    return this.list[index].getType();
  }

  getUntitledCount() {
    return this.untitledCount;
  }

  getUrl(index) {
    return this.list[index].getUrl();
  }

  getValue(index) {
    return this.list[index].getValue();
  }

  hasRedo(index) {
    return this.list[index].hasRedo();
  }

  hasUndo(index) {
    return this.list[index].hasUndo();
  }

  isClean(index) {
    return this.list[index].isClean();
  }

  isCoreFile(index) {
    return this.list[index].isCoreFile();
  }

  isEmptyFile(index) {
    return this.list[index].isEmptyFile();
  }

  isReadOnly(index) {
    return this.list[index].isReadOnly();
  }

  incUntitledCount() {
    this.untitledCount++;
  }

  markClean(index) {
    return this.list[index].markClean();
  }

  setActive(index) {
    this.active = index;
    if (this.list.length === 0) {
      this.active = -1;
      return;
    }
    if (this.active >= this.list.length) {
      this.active = this.list.length - 1;
      return;
    }
    if (this.active < 0) {
      this.active = 0;
    }
  }

  setDisplayName(index, displayName) {
    this.list[index].setDisplayName(displayName);
  }

  setEditSession(index, editSession) {
    this.list[index].setEditSession(editSession);
  }

  setFileHandle(index, fileHandle) {
    this.list[index].setFileHandle(fileHandle);
  }

  setMode(index, mode) {
    this.list[index].setMode(mode);
  }

  setName(index, name) {
    this.list[index].setName(name);
  }

  setReadOnly(index, readOnly) {
    this.list[index].setReadOnly(readOnly);
  }

  setType(index, type) {
    this.list[index].setType(type);
  }

  setUntitledName(index) {
    const name = STRINGS.UNTITLED + '_' + this.getUntitledCount() + '.txt';
    this.incUntitledCount();
    this.setName(index, name);
    this.setDisplayName(index, name);
    this.setUrl(index, this.createUrl(false, name));
  }

  setUrl(index, url) {
    this.list[index].setUrl(url);
  }

  setValue(index, text) {
    this.list[index].setValue(text);
  }

  createFileData() {
    for (let i = 0; i < this.list.length; i++) {
      if (this.isEmptyFile(i)) {
        return i;
      }
    }
    return this.list.push(
      new FileData({FILE_URL: FILE_URL, FILES: FILES})
    ) - 1;
  }

  dropFileData(index) {
    this.list.splice(index, 1);
    this.setActive(index);
  }

  correctExt(path) {
    const extList = [
      ['webmanifest', 'json'],
    ];
    for (let extData of extList) {
      if (path.endsWith('.' + extData[0])) {
        return path.slice(0, path.length - extData[0].length) + extData[1];
      }
    }
    return path;
  }

  getModeForPath(name) {
    const modeList = ace.require('ace/ext/modelist');
    return modeList.getModeForPath('./' + name).mode;
  }

  async findFileHandleIndexA(fileHandle) {
    for (let i = 0; i < this.list.length; i++) {
      if (this.getFileHandle(i) &&
          await fileHandle.isSameEntry(this.getFileHandle(i))) {
        return i;
      }
    }
    return -1;
  }

  findIndex(href) {
    return this.list.findIndex(fileData => fileData.url.href == href);
  }

  createUrl(exists, name) {
    return new URL(
      FILE_URL.PROTOCOL + '//' + location.host + '/' +
      (exists ? FILE_URL.DIRS.EXISTS : FILE_URL.DIRS.INTERNAL) + '/' + name
    );
  }

  async openFileA(index, file, fileDecoding = 'utf-8') {
    let text = null;
    try {
      text = await this.fileHelper.fetchTextFileA(file, fileDecoding);
      if (text === null) {
        throw new Error();
      }
    } catch (error) {
      console.error(error.name, error.message);
      return {success: false, message: 'failed: ' + file.name};
    }
    this.setDisplayName(index, file.name);
    this.setMode(index, this.getModeForPath(this.correctExt(file.name)));
    this.setName(index, file.name);
    this.setType(index, file.type);
    this.setUrl(index, this.createUrl(true, file.name));
    this.setValue(index, text);
    return {success: true, message: 'success: ' + file.name};
  }

  async openHandleA(index, fileHandle, fileDecoding = 'utf-8') {
    let file = null;
    let text = null;
    try {
      file = await fileHandle.getFile();
      text = await this.fileHelper.readTextFileA(file, fileDecoding);
      if (text === null) {
        throw new Error();
      }
    } catch (error) {
      console.error(error.name, error.message);
      let name = 'null';
      if (file.name) {
        name = file.name;
      }
      return {success: false, message: 'failed: ' + name};
    }
    this.setDisplayName(index, file.name);
    this.setFileHandle(index, fileHandle);
    this.setMode(index, this.getModeForPath(this.correctExt(file.name)));
    this.setName(index, file.name);
    this.setType(index, 'text/plain');
    this.setUrl(index, this.createUrl(true, file.name));
    this.setValue(index, text);
    return {success: true, message: 'success: ' + file.name};
  }

  async openUrlA(index, url) {
    let text = null;
    try {
      if (url.href.includes(FILES.CACHE_LIST)) {
        const cacheManager = new CacheManager({STRINGS: STRINGS});
        text = await cacheManager.formattedCacheListA('markdown');
      } else {
        text = await this.fileHelper.fetchTextUrlA(url, 'utf-8');
        if (text === null) {
          throw new Error();
        }
      }
    } catch (error) {
      console.error(error.name, error.message);
      return {success: false, message: 'failed: ' + url.pathname};
    }
    this.setDisplayName(index, url.pathname);
    this.setMode(index, this.getModeForPath(this.correctExt(url.pathname)));
    this.setName(index, url.pathname);
    this.setType(index, 'text/plain');
    this.setValue(index, text);
    return {success: true, message: 'success: ' + url.pathname};
  }

  downloadFile(index) {
    const blob = this.fileHelper.buildBlob(
      this.getValue(index), this.getType(index)
    );
    this.fileHelper.downloadBlob(blob, this.getName(index));
    return true;
  }

  async saveFileA(index) {
    if (!this.getFileHandle(index)) {
      return false;
    }
    const blob = this.fileHelper.buildBlob(
      this.getValue(index), this.getType(index)
    );
    try {
      await this.fileHelper.writeBlobA(blob, this.getFileHandle(index));
    } catch (error) {
      console.error(error.name, error.message);
      return false;
    }
    return true;
  }

  renameFile(index, name) {
    const sanitizedName = this.fileHelper.sanitizeName(name);
    this.setName(index, sanitizedName);
    this.setDisplayName(index, sanitizedName);
    this.setUrl(index, this.createUrl(true, sanitizedName));
    this.setType(index, 'text/plain');
    this.setMode(index, this.getModeForPath(this.correctExt(sanitizedName)));
  }
}
