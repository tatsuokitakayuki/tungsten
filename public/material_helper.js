import {HtmlHelper} from '/html_helper.js';

export class MaterialHelper {
  constructor() {
    this.htmlHelper = HtmlHelper.newHelper();
  }

  underline() {
    return this.htmlHelper.div('', [], ['mdc-line-ripple']);
  }

  listItem(id, text, meta) {
    const elements = [this.listItemRipple()];
    if (text) {
      elements.push(this.listItemText(text));
    }
    if (meta) {
      elements.push(this.listItemMeta(meta));
    }
    return this.htmlHelper.li(
      elements,
      [{name: 'id', value: id}, {name: 'tabindex', value: '-1'}],
      ['mdc-list-item']
    );
  }

  listItemRipple() {
    return this.htmlHelper.span('', [], ['mdc-list-item__ripple']);
  }

  listItemText(text) {
    return this.htmlHelper.span(text, [], ['mdc-list-item__text']);
  }

  listItemMeta(text) {
    return this.htmlHelper.span(text, [], ['mdc-list-item__meta']);
  }

  listItemIcon(name) {
    return this.htmlHelper.span(
      name,
      [
        {name: 'role', value: 'button'},
        {name: 'aria-hidden', value: 'true'}
      ],
      ['mdc-list-item__graphic', 'material-icons']
    );
  }

  menuItem(item) {
    let li = this.listItem(item.id, item.text, item.meta);
    li.setAttribute('role', 'menuitem');
    return li;
  }

  buttonRipple() {
    return this.htmlHelper.div('', [], ['mdc-button__ripple']);
  }

  buttonTouch() {
    return this.htmlHelper.div('', [], ['mdc-button__touch']);
  }

  buttonLabel(label) {
    return this.htmlHelper.span(label, [], ['mdc-button__label']);
  }

  buttonIcon(iconName) {
    return this.htmlHelper.span(
      iconName,
      [{name: 'aria-hidden', value: 'true'}],
      ['material-icons', 'mdc-button__icon']
    );
  }

  icon(name) {
    return this.htmlHelper.span(
      name, [{name: 'aria-hidden', value: 'true'}], ['material-icons']
    );
  }

  buttonSnackbar() {
    return this.htmlHelper.button(
      [this.buttonRipple()], [{name: 'type', value: 'button'}], ['mdc-button']
    );
  }

  actionSnackbar() {
    const action = this.buttonSnackbar();
    action.classList.add('mdc-snackbar__action');
    return action;
  }

  closeSnackbar() {
    const action = this.buttonSnackbar();
    action.classList.add('mdc-snackbar__dismiss');
    const span = this.htmlHelper.span(
      'close',
      [
        {name: 'aria-hidden', value: 'true'},
        {name: 'style', value: 'color: white;'}
      ],
      ['mdc-button__label', 'material-icons']
    );
    action.appendChild(span);
    return action;
  }

  buttonDialog(label) {
    return this.htmlHelper.button(
      [
        this.buttonRipple(),
        this.htmlHelper.span(label, [], ['mdc-button__label'])
      ],
      [
        {name: 'type', value: 'button'},
        {name: 'role', value: 'button'},
        {name: 'tabindex', value: '0'}
      ],
      ['mdc-button', 'mdc-dialog__button', 'mdc-button--outlined']
    );
  }

  buttonDialogOk(resources) {
    const button = this.buttonDialog(resources.BUTTONS.OK);
    button.setAttribute('id', 'dialog-button-ok');
    button.setAttribute('data-mdc-dialog-action', 'submit');
    button.setAttribute('data-mdc-dialog-button-default', '');
    return button;
  }

  buttonDialogCancel(resources) {
    const button = this.buttonDialog(resources.BUTTONS.CANCEL);
    button.setAttribute('id', 'dialog-button-cancel');
    button.setAttribute('data-mdc-dialog-action', 'reset');
    return button;
  }

  list(liList, id = 'list') {
    return this.htmlHelper.ul(
      liList, [{name: 'id', value: id}], ['mdc-list', 'mdc-list--dense']
    );
  }

  notchedOutline() {
    return this.htmlHelper.span(
      [
        this.htmlHelper.span('', [], ['mdc-notched-outline__leading']),
        this.htmlHelper.span('', [], ['mdc-notched-outline__trailing'])
      ],
      [],
      ['mdc-notched-outline']
    );
  }
}
