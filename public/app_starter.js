import {HtmlHelper} from '/html_helper.js';
import '/localForage/localforage.min.js';
import '/emmet-core/emmet.min.js';
import '/ace/ace.js';
ace.config.set('basePath', new URL('ace', location.href).href);
import '/ace/ext-code_lens.js';
import '/ace/ext-elastic_tabstops_lite.js';
import '/ace/ext-emmet.js';
import '/ace/ext-language_tools.js';
import '/ace/ext-linking.js';

const htmlHelper = HtmlHelper.newHelper();
let newWorker = null;

const getStringsA = async () => {
  const {Resources} = await import('/resources.js');
  return await new Resources().getJsonA('strings.json');
};

const onDOMContentLoadedA = async () => {
  htmlHelper.removeChildren(htmlHelper.elementById('editor'));
  const {Core} = await import('/core.js');
  await new Core().initializeA();
};

const onStateChangeA = async () => {
  switch (newWorker.state) {
    case 'installed':
      if (navigator.serviceWorker.controller) {
        const {ChangeSnackbarEvent} = await import(
          '/change_snackbar_event.js'
        );
        const STRINGS = await getStringsA();
        document.dispatchEvent(
          new ChangeSnackbarEvent(STRINGS.RESTART_UPDATE, true, null)
        );
      }
      break;
    default:
      break;
  }
};

const onUpdateFound = reg => {
  newWorker = reg.installing;
  htmlHelper.listen(
    'statechange', async () => await onStateChangeA(), {}, newWorker
  );
};

const onRegistered = reg => {
  console.log('Service worker has been registered for scope: ' + reg.scope);
  htmlHelper.listen('updatefound', () => onUpdateFound(reg), {}, reg);
};

const onLoadA = async () =>
  onRegistered(await navigator.serviceWorker.register('/sw.js', {scope: '/'}));

const alertNotCompatibleA = async () => {
  const STRINGS = await getStringsA();
  window.alert(STRINGS.NOT_COMPATIBLE);
};

htmlHelper.listen(
  'DOMContentLoaded', async () => await onDOMContentLoadedA(), {}, window
);
if ('serviceWorker' in navigator) {
  htmlHelper.listen('load', async () => await onLoadA(), {}, window);
} else {
  alertNotCompatibleA();
}
