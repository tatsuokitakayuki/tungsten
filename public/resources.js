import {FileHelper} from '/file_helper.js';

export class Resources {
  async getJsonA(name) {
    const url = new URL(`res/${name}`, location.href);
    const fileHelper = FileHelper.newHelper();
    const json = await fileHelper.fetchTextUrlA(url, 'utf-8');
    return JSON.parse(json);
  }
}
