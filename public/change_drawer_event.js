export class ChangeDrawerEvent extends CustomEvent {
  constructor() {
    super('Drawer:change');
  }
}
