import {ChangeDrawerEvent} from '/change_drawer_event.js';
import {ChangeSnackbarEvent} from '/change_snackbar_event.js';
import {ChangeViewEvent} from '/change_view_event.js';
import {SaveOptionsEvent} from '/save_options_event.js';

let DESCRIPTIONS = null;
let FILES = null;
let REPOSITORY = null;

export class Keybinding {
  constructor(core) {
    this.core = core;
    this.appChangeLog = null;
    this.appOpenSourceLicense = null;
    this.appPermissions = null;
    this.appShortcutList = null;
    this.appSourceCode = null;
  }

  initializeFileList(editor) {
    const commands = [
      {
        name: 'appNextFile',
        description: DESCRIPTIONS.NEXT_FILE,
        bindKey: {win: 'Ctrl-Tab', mac: 'Command-Tab'},
        exec: editor => this.changeFile(editor, 'next'),
        readOnly: true
      },
      {
        name: 'appPreviousFile',
        description: DESCRIPTIONS.PREVIOUS_FILE,
        bindKey: {win: 'Ctrl-Shift-Tab', mac: 'Command-Shift-Tab'},
        exec: editor => this.changeFile(editor, 'previous'),
        readOnly: true
      },
      {
        name: 'appSelectFile1',
        description: DESCRIPTIONS.FIRST_FILE,
        bindKey: {win: 'Ctrl-1', mac: 'Command-1'},
        exec: editor => this.changeFile(editor, 0),
        readOnly: true
      },
      {
        name: 'appSelectFile2',
        description: DESCRIPTIONS.SECOND_FILE,
        bindKey: {win: 'Ctrl-2', mac: 'Command-2'},
        exec: editor => this.changeFile(editor, 1),
        readOnly: true
      },
      {
        name: 'appSelectFile3',
        description: DESCRIPTIONS.THIRD_FILE,
        bindKey: {win: 'Ctrl-3', mac: 'Command-3'},
        exec: editor => this.changeFile(editor, 2),
        readOnly: true
      },
      {
        name: 'appSelectFile4',
        description: DESCRIPTIONS.FOURTH_FILE,
        bindKey: {win: 'Ctrl-4', mac: 'Command-4'},
        exec: editor => this.changeFile(editor, 3),
        readOnly: true
      },
      {
        name: 'appSelectFile5',
        description: DESCRIPTIONS.FIFTH_FILE,
        bindKey: {win: 'Ctrl-5', mac: 'Command-5'},
        exec: editor => this.changeFile(editor, 4),
        readOnly: true
      },
      {
        name: 'appSelectFile6',
        description: DESCRIPTIONS.SIXTH_FILE,
        bindKey: {win: 'Ctrl-6', mac: 'Command-6'},
        exec: editor => this.changeFile(editor, 5),
        readOnly: true
      },
      {
        name: 'appSelectFile7',
        description: DESCRIPTIONS.SEVENTH_FILE,
        bindKey: {win: 'Ctrl-7', mac: 'Command-7'},
        exec: editor => this.changeFile(editor, 6),
        readOnly: true
      },
      {
        name: 'appSelectFile8',
        description: DESCRIPTIONS.EIGHTH_FILE,
        bindKey: {win: 'Ctrl-8', mac: 'Command-8'},
        exec: editor => this.changeFile(editor, 7),
        readOnly: true
      },
      {
        name: 'appSelectFileLast',
        description: DESCRIPTIONS.LAST_FILE,
        bindKey: {win: 'Ctrl-9', mac: 'Command-9'},
        exec: editor => this.changeFile(editor, 'last'),
        readOnly: true
      },
    ];
    this.addCommands(editor, commands);
  }

  initializeFileMenu(editor) {
    let commands = [
      {
        name: 'appNewFile',
        description: DESCRIPTIONS.NEW_FILE,
        bindKey: {win: 'Ctrl-N', mac: 'Command-N'},
        exec: async () => await this.newFileA(),
        readOnly: true
      },
      {
        name: 'appOpenFile',
        description: DESCRIPTIONS.OPEN_FILE + '...',
        bindKey: {win: 'Ctrl-O', mac: 'Command-O'},
        exec: async () => await this.openSelectFileA(),
        readOnly: true
      },
      {
        name: 'appSaveFile',
        description: DESCRIPTIONS.SAVE_FILE,
        bindKey: {win: 'Ctrl-S', mac: 'Command-S'},
        exec: async () => await this.saveFileA(),
        readOnly: false
      },
      {
        name: 'appRenameFile',
        description: DESCRIPTIONS.RENAME_FILE + '...',
        bindKey: {win: 'Alt-R', mac: 'Option-R'},
        exec: async () => await this.renameFileA(),
        readOnly: false
      },
      {
        name: 'appFileDecoding',
        description: DESCRIPTIONS.FILE_DECODING + '...',
        exec: async () => await this.fileDecodingA(),
        readOnly: true
      },
      {
        name: 'appSaveEditSessionOptions',
        description: DESCRIPTIONS.SAVE_EDIT_SESSION_OPTIONS,
        exec: editor => this.saveEditSessionOptions(editor),
        readOnly: true
      },
      {
        name: 'appCloseFile',
        description: DESCRIPTIONS.CLOSE_FILE + '...',
        bindKey: {win: 'Alt-W', mac: 'Option-W'},
        exec: async () => await this.closeFileA(),
        readOnly: true
      },
      {
        name: 'appQuitApp',
        description: DESCRIPTIONS.QUIT_APP,
        bindKey: {win: 'Ctrl-W', mac: 'Command-W'},
        exec: () => this.quitApp(),
        readOnly: true
      },
    ];
    if (this.getWindowStyle() !== 'standalone') {
      commands = commands.filter(item => item.name !== 'appQuitApp');
    }
    this.addCommands(editor, commands);
  }

  initializeEditMenu(editor) {
    this.removeCommands(editor, ['tolowercase']);
    const commands = [
      {
        name: 'appToLowercase',
        description: DESCRIPTIONS.TO_LOWERCASE,
        bindKey: {win: 'Ctrl-Shift-U|Alt-U', mac: 'Ctrl-Shift-U'},
        exec: editor => editor.toLowerCase(),
        readOnly: false
      },
    ];        
    this.addCommands(editor, commands);
  }

  initializeHelpMenu(editor) {
    const commands = [
      {
        name: 'appShortcutList',
        description: DESCRIPTIONS.SHORTCUT_LIST,
        bindKey: {win: "Ctrl-Alt-h", mac: "Command-Option-h"},
        exec: () => this.openUrl(
          this.appShortcutList,
          REPOSITORY.BASE + REPOSITORY.DIR.MASTER + '/' +
          FILES.SHORTCUT_LIST + '.md',
          'appShortcutList'
        ),
        readOnly: true
      },
      {
        name: 'appExportOptions',
        description: DESCRIPTIONS.EXPORT_OPTIONS,
        exec: async editor => await this.exportOptionsA(editor),
        readOnly: true
      },
      {
        name: 'appImportOptions',
        description: DESCRIPTIONS.IMPORT_OPTIONS + '...',
        exec: async editor => await this.importOptionsA(editor),
        readOnly: true
      },
      {
        name: 'appCacheList',
        description: DESCRIPTIONS.CACHE_LIST,
        exec: async () => await this.cacheListA(),
        readOnly: true
      },
      {
        name: 'appSourceCode',
        description: DESCRIPTIONS.SOURCE_CODE,
        exec: () => this.openUrl(
            this.appSourceCode, REPOSITORY.BASE, 'appSourceCode'
        ),
        readOnly: true
      },
      {
        name: 'appPermissions',
        description: DESCRIPTIONS.PERMISSIONS,
        exec: () => this.openUrl(
          this.appPermissions,
          REPOSITORY.BASE + REPOSITORY.DIR.MASTER + '/' +
          FILES.PERMISSIONS + '.md',
          'appPermissions'
        ),
        readOnly: true
      },
      {
        name: 'appChangeLog',
        description: DESCRIPTIONS.CHANGE_LOG,
        exec: () => this.openUrl(
          this.appChangeLog,
          REPOSITORY.BASE + REPOSITORY.DIR.MASTER + '/' +
          FILES.CHANGE_LOG + '.md',
          'appChangeLog'
        ),
        readOnly: true
      },
      {
        name: 'appOpenSourceLicense',
        description: DESCRIPTIONS.OPEN_SOURCE_LICENSE,
        exec: () => this.openUrl(
          this.appOpenSourceLicense,
          REPOSITORY.BASE + REPOSITORY.DIR.MASTER + '/' +
          FILES.OPEN_SOURCE_LICENSE + '.md',
          'appOpenSourceLicense'
        ),
        readOnly: true
      },
      {
        name: 'appAbout',
        description: DESCRIPTIONS.ABOUT,
        exec: async () => await this.aboutA(),
        readOnly: true
      },
    ];
    this.addCommands(editor, commands);
  }

  initializeTopAppBar(editor) {
    const commands = [
      {
        name: 'appToggleFileList',
        description: DESCRIPTIONS.TOGGLE_FILE_LIST,
        bindKey: {win: 'Ctrl-Shift-F', mac: 'Command-Shift-F'},
        exec: () => document.dispatchEvent(new ChangeDrawerEvent()),
        readOnly: true
      },
    ];
    this.addCommands(editor, commands);
  }

  initialize(editor, resources) {
    DESCRIPTIONS = resources.DESCRIPTIONS;
    FILES = resources.FILES;
    REPOSITORY = resources.REPOSITORY;
    this.initializeFileList(editor);
    this.initializeFileMenu(editor);
    this.initializeEditMenu(editor);
    this.initializeHelpMenu(editor);
    this.initializeTopAppBar(editor);
  }

  getActive() {
    return this.core.getActive();
  }

  getLength() {
    return this.core.getLength();
  }

  getOption(name) {
    return this.core.getOption(name);
  }

  getWindowStyle() {
    return this.core.getWindowStyle();
  }

  setActive(index) {
    this.core.setActive(index);
  }

  async newFileA() {
    await this.core.newFileA();
  }

  async openSelectFileA() {
    await this.core.openSelectFileA();
  }

  async saveFileA() {
    await this.core.saveFileA(this.getActive());
  }

  async renameFileA() {
    await this.core.renameFileA(this.getActive(), null, null);
  }

  async fileDecodingA() {
    await this.core.fileDecodingA();
  }

  saveEditSessionOptions(editor) {
    document.dispatchEvent(
      new SaveOptionsEvent(editor, editor.session, {session: true, app: true})
    );
    document.dispatchEvent(
      new ChangeSnackbarEvent('Saved edit session options.', true, null)
    );
  }

  async closeFileA() {
    await this.core.closeFileA(this.getActive());
  }

  quitApp() {
    try {
      window.close();
    } catch (error) {
      console.error(error.name, error.message);
    }
  }

  async exportOptionsA(editor) {
    await this.core.exportOptionsA(editor, editor.session);
  }

  async importOptionsA(editor) {
    await this.core.importOptionsA(editor, editor.session);
  }

  async cacheListA() {
    await this.core.cacheListA();
  }

  async aboutA() {
    await this.core.aboutA();
  }

  addCommands(editor, commands) {
    editor.commands.addCommands(commands);
  }

  removeCommands(editor, commandNames) {
    for (const commandName of commandNames) {
      editor.commands.removeCommand(commandName);
    }
  }

  changeFile(editor, to) {
    const from = this.getActive();
    const length = this.getLength();
    if (to == 'next') {
      to = from + 1;
      if (to >= length) {
        to = 0;
      }
    }
    if (to == 'previous') {
      to = from - 1;
      if (to < 0) {
        to = length - 1;
      }
    }
    if (to == 'last') {
      to = length - 1;
    }
    if (to >= length) {
      return;
    }
    this.setActive(to);
    document.dispatchEvent(
      new ChangeViewEvent({from: from, to: to}, {all: true})
    );
  }

  openUrl(appWindow, url, windowName) {
    if (appWindow == null || appWindow.closed) {
      appWindow = window.open(url, windowName);
    } else {
      appWindow.focus();
    }
  }
}
