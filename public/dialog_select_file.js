import {HtmlHelper} from '/html_helper.js';

const selectFileA = multiple => new Promise(resolve => {
  const htmlHelper = HtmlHelper.newHelper();
  let attributes = [{name: 'type', value: 'file'}];
  if (multiple) {
    attributes.push({name: 'multiple', value: ''});
  }
  const input = htmlHelper.input('', attributes);
  input.onchange = event => resolve(event.target.files);
  input.click();
});

export class DialogSelectFile {
  async openA(multiple) {
    return await selectFileA(multiple);
  }
}
