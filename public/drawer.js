import {ChangeEditorOptionEvent} from '/change_editor_option_event.js';
import {ChangeViewEvent} from '/change_view_event.js';
import {FocusEditorEvent} from '/focus_editor_event.js';
import {HtmlHelper} from '/html_helper.js';
import {MaterialHelper} from '/material_helper.js';
import {StatusHelper} from '/status_helper.js';

const MDCDrawer = mdc.drawer.MDCDrawer;

export class Drawer {
  constructor(core) {
    this.core = core;
    this.htmlHelper = HtmlHelper.newHelper();
    this.drawer = new MDCDrawer(this.htmlHelper.elementById('drawer-view'));
    this.fileListView = this.htmlHelper.elementById('file-list');
  }

  initialize() {
    this.fileListView.singleSelection = true;
    this.fileListView.wrapFocus = true;
    this.htmlHelper.listen(
      'MDCList:action',
      event => this.onAction(event),
      {passive: true},
      this.fileListView
    );
    this.htmlHelper.listen(
      'Drawer:changeitem',
      event => this.onChangeItem(event),
      {passive: true},
      document
    );
    this.htmlHelper.listen(
      'Drawer:change', () => this.onChange(), {passive: true}, document
    );
  }

  onAction(event) {
    const {index} = event.detail;
    const from = this.getActive();
    this.setActive(index);
    document.dispatchEvent(
      new ChangeViewEvent({from: from, to: index}, {all: true})
    );
    this.focusEditor();
  }

  onChangeItem(event) {
    const length = this.fileListView.childNodes.length;
    if (!length) {
      return;
    }
    const {from, to} = event.detail;
    if (from >= length || to >= length) {
      return;
    }
    this.updateItem(from, false);
    this.updateItem(to, true);
  }

  onChange() {
    const opened = !this.drawer.open;
    const target = this.htmlHelper.elementById('drawer-view');
    if (opened) {
      target.classList.add('mdc-drawer--open');
    } else {
      target.classList.remove('mdc-drawer--open');
    }
    document.dispatchEvent(new ChangeEditorOptionEvent('drawerOpened', opened));
    this.getEditor().resize();
    this.focusEditor();
  }

  getActive() {
    return this.core.getActive();
  }

  getDisplayName(index) {
    return this.core.getDisplayName(index);
  }

  getEditor() {
    return this.core.getEditor();
  }

  getFileData(index) {
    return this.core.getFileData(index);
  }

  getIconName(index) {
    let iconName = 'insert_drive_file';
    if (this.isCoreFile(index)) {
      iconName = 'info';
    }
    return iconName;
  }

  getTime(index) {
    return this.core.getTime(index);
  }

  getUrl(index) {
    return this.core.getUrl(index);
  }

  hasItem(index) {
    return Boolean(this.fileListView.childNodes[index]);
  }

  isCoreFile(index) {
    return this.core.isCoreFile(index);
  }

  setActive(index) {
    this.core.setActive(index);
  }

  addItem(index) {
    const item = this.buildItem(index, false);
    this.fileListView.appendChild(item);
  }

  buildItem(index, selected) {
    const materialHelper = new MaterialHelper();
    const fileData = this.getFileData(index);
    const statusHelper = new StatusHelper({});
    const id = 'file-list-item-' + String(this.getTime(index)) + '-' +
        String(index);
    const item = materialHelper.menuItem({id: id});
    if (selected) {
      item.classList.add('mdc-list-item--selected');
    }
    item.setAttribute('aria-selected', String(selected));
    item.setAttribute('title', this.getUrl(index).href);
    item.appendChild(materialHelper.listItemIcon(this.getIconName(index)));
    const text = statusHelper.buildDisplayNameText(
      this.getDisplayName(index), fileData
    );
    item.appendChild(materialHelper.listItemText(text));
    return item;
  }

  removeItem(index) {
    this.fileListView.removeChild(this.fileListView.childNodes[index]);
  }

  updateItem(index, selected) {
    const newItem = this.buildItem(index, selected);
    this.fileListView.replaceChild(
      newItem, this.fileListView.childNodes[index]
    );
    if (selected) {
      this.fileListView.childNodes[index].scrollIntoView(
        {behavior: 'auto', block: 'center'}
      );
    }
  }

  focusEditor() {
    document.dispatchEvent(new FocusEditorEvent());
  }
}
