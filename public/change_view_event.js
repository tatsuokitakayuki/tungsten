export class ChangeViewEvent extends CustomEvent {
  constructor(fromTo, flags) {
    super('View:change', {detail: {fromTo: fromTo, flags: flags}});
  }
}
