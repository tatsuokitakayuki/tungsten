export class ChangeStatusbarEvent extends CustomEvent {
  constructor(name, editor, fileData) {
    super(
      'Statusbar:change',
      {detail: {name: name, editor: editor, fileData: fileData}}
    );
  }
}
