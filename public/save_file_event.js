export class SaveFileEvent extends CustomEvent {
  constructor() {
    super('File:save');
  }
}
