import {HtmlHelper} from '/html_helper.js';
import {MaterialHelper} from '/material_helper.js';

const MDCSnackbar = mdc.snackbar.MDCSnackbar;

export class Snackbar {
  constructor() {
    this.htmlHelper = HtmlHelper.newHelper();
    this.materialHelper = new MaterialHelper();
    this.snackbar = new MDCSnackbar(
      this.htmlHelper.elementById('snackbar-view')
    );
    this.que = [];
    this.command = null;
  }

  initialize() {
    this.snackbar.listen(
      'MDCSnackbar:closed', event => this.onClosed(event), {passive: true}
    );
    this.htmlHelper.listen(
      'Snackbar:change',
      event => this.onChange(event),
      {passive: true},
      document
    );
  }

  onClosed(event) {
    const {reason} = event;
    if (reason && reason == 'action') {
      this.command();
    }
    this.snackbar.labelText = '';
    const snackbarActions = this.htmlHelper.elementById('snackbar-actions');
    this.htmlHelper.removeChildren(snackbarActions);
    this.command = null;
    const next = this.que.shift();
    if (next) {
      this.update(next.label, next.close, next.action);
    }
  }

  onChange(event) {
    const {label, close, action} = event.detail;
    if (this.snackbar.isOpen) {
      this.que.push({label: label, close: close, action: action});
      return;
    }
    this.update(label, close, action);
  }

  update(label, close, action) {
    this.snackbar.labelText = label;
    this.snackbar.closeOnEscape = true;
    this.snackbar.timeoutMs = 5000;
    if (action) {
      this.setAction(action);
    }
    if (close) {
      this.setClose();
    }
    this.snackbar.open();
  }

  setAction(action) {
    const snackbarActions = this.htmlHelper.elementById('snackbar-actions');
    const button = this.materialHelper.actionSnackbar();
    button.appendChild(
      this.htmlHelper.span(
        action.label, [{name: 'style', value: 'color: magenta;'}]
      )
    );
    button.classList.add('mdc-button__label');
    snackbarActions.appendChild(button);
    this.command = action.command;
  }

  setClose() {
    const snackbarActions = this.htmlHelper.elementById('snackbar-actions');
    snackbarActions.appendChild(this.materialHelper.closeSnackbar());
  }
}
