export class HtmlHelper {
  static newHelper() {
    return new HtmlHelper();
  }

  elementById(id) {
    return document.getElementById(id);
  }

  setElements(target, elements = '') {
    if (!Array.isArray(elements)) {
      target.textContent = elements;
    } else {
      for (const item of elements) {
        target.appendChild(item);
      }
    }
  }

  setAttributes(target, attributes = []) {
    for (const item of attributes) {
      target.setAttribute(item.name, item.value);
    }
  }

  setClasses(target, classes = []) {
    target.classList.add(...classes);
  }

  element(name, elements, attributes, classes) {
    const target = document.createElement(name);
    this.setElements(target, elements);
    this.setAttributes(target, attributes);
    this.setClasses(target, classes);
    return target;
  }

  a(elements, attributes, classes) {
    return this.element('a', elements, attributes, classes);
  }

  button(elements, attributes, classes) {
    return this.element('button', elements, attributes, classes);
  }

  div(elements, attributes, classes) {
    return this.element('div', elements, attributes, classes);
  }

  input(elements, attributes, classes) {
    return this.element('input', elements, attributes, classes);
  }

  label(elements, attributes, classes) {
    return this.element('label', elements, attributes, classes);
  }

  li(elements, attributes, classes) {
    return this.element('li', elements, attributes, classes);
  }

  optgroup(elements, attributes, classes) {
    return this.element('optgroup', elements, attributes, classes);
  }

  option(elements, attributes, classes) {
    return this.element('option', elements, attributes, classes);
  }

  p(elements, attributes, classes) {
    return this.element('p', elements, attributes, classes);
  }

  select(elements, attributes, classes) {
    return this.element('select', elements, attributes, classes);
  }

  span(elements, attributes, classes) {
    return this.element('span', elements, attributes, classes);
  }

  ul(elements, attributes, classes) {
    return this.element('ul', elements, attributes, classes);
  }

  removeChildren(target) {
    while (target.firstChild) {
      this.removeChildren(target.firstChild);
      target.removeChild(target.firstChild);
    }
  }

  listen(type, listener, options, target) {
    target.addEventListener(type, listener, options);
  }
}
