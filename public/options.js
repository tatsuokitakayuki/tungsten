import {CacheManager} from '/cache_manager.js';
import {FileHelper} from '/file_helper.js';
import {HtmlHelper} from '/html_helper.js';

let EDITOR_OPTIONS = null;
let STRINGS = null;

export class Options {
  constructor() {
    this.app = null;
    this.fileHelper = FileHelper.newHelper();
  }

  async initializeA(editor, editSession, resources) {
    EDITOR_OPTIONS = resources.EDITOR_OPTIONS;
    STRINGS = resources.STRINGS;
    this.app = EDITOR_OPTIONS.APP;
    await this.initializeAppOptionsA();
    await this.initializeEditorOptionsA(editor);
    await this.initializeSessionOptionsA(editSession);
    await this.initializeExtensionsOptionsA(editor);
    const htmlHelper = HtmlHelper.newHelper();
    htmlHelper.listen(
      'Options:save',
      event => this.onSaveOptions(event),
      {passive: true},
      document
    );
  }

  onSaveOptions(event) {
    const {editor, editSession, options} = event.detail;
    if (options.all || options.editor) {
      this.saveEditorOptions(editor);
    }
    if (options.all || options.session) {
      this.saveSessionOptions(editSession);
    }
    if (options.all || options.extensions) {
      this.saveExtensionsOptions(editor);
    }
    if (options.all || options.app) {
      this.saveAppOptions();
    }
  }

  async initializeAppOptionsA() {
    const value = await localforage.getItem('options_app');
    if (value) {
      Object.assign(this.app, value);
    }
  }

  async initializeEditorOptionsA(editor) {
    let value = await localforage.getItem('options_editor');
    if (!value) {
      value = EDITOR_OPTIONS.EDITOR;
    }
    editor.setOptions(value);
  }

  async initializeSessionOptionsA(editSession) {
    let value = await localforage.getItem('options_session');
    if (!value) {
      value = EDITOR_OPTIONS.SESSION;
    }
    editSession.setOptions(value);
  }

  async initializeExtensionsOptionsA(editor) {
    let value = await localforage.getItem('options_extensions');
    if (!value) {
      value = EDITOR_OPTIONS.EXTENSIONS;
    }
    editor.setOptions(value);
  }

  getAppOptions() {
    return this.app;
  }

  getEditorOptions(editor) {
    return Object.fromEntries(
      Object.keys(EDITOR_OPTIONS.EDITOR).map(
        key => [key, editor.getOption(key)]
      )
    );
  }

  getSessionOptions(editSession) {
    return Object.fromEntries(
      Object.keys(EDITOR_OPTIONS.SESSION).map(
        key => [key, editSession.getOption(key)]
      )
    );
  }

  getExtensionsOptions(editor) {
    return Object.fromEntries(
      Object.keys(EDITOR_OPTIONS.EXTENSIONS).map(
        key => [key, editor.getOption(key)]
      )
    );
  }

  saveAppOptions() {
    localforage.setItem('options_app', this.getAppOptions());
  }

  saveEditorOptions(editor) {
    localforage.setItem('options_editor', this.getEditorOptions(editor));
  }

  saveSessionOptions(editSession) {
    localforage.setItem('options_session', this.getSessionOptions(editSession));
  }

  saveExtensionsOptions(editor) {
    localforage.setItem(
      'options_extensions', this.getExtensionsOptions(editor)
    );
  }

  async exportOptionsA(editor, editSession) {
    const cacheManager = new CacheManager({STRINGS: STRINGS});
    const appVersion = await cacheManager.getVersionA(STRINGS.APP_CACHE_NAME, 0);
    const options = {
      info_options: {
        exportVersion: '1',
        exportDate: new Date().toString(),
        appName: STRINGS.APP_NAME,
        appVersion: appVersion,
      },
      options: {
        app: this.getAppOptions(),
        editor: this.getEditorOptions(editor),
        session: this.getSessionOptions(editSession),
        extensions: this.getExtensionsOptions(editor)
      }
    };
    const tabSize = editSession.getOption('tabSize');
    const json = JSON.stringify(options, null, tabSize);
    const blob = this.fileHelper.buildBlob(json, 'application/json');
    this.fileHelper.downloadBlob(blob, 'export_options.json');
  }

  async importOptionsA(file, editor, editSession) {
    const json = await this.fileHelper.fetchTextFileA(file);
    const options = JSON.parse(json).options;
    if (options.editor) {
      editor.setOptions(options.editor);
    }
    if (options.renderer) {
      editor.setOptions(options.renderer);
    }
    if (options.session) {
      editSession.setOptions(options.session);
    }
    if (options.extensions) {
      editor.setOptions(options.extensions);
    }
    Object.assign(this.app, options.app);
  }
}
