import {DialogPrompt} from '/dialog/dialog_prompt.js';
import {RenameFileEvent} from '/rename_file_event.js';

let DESCRIPTIONS = null;
let PLACEHOLDERS = null;

export class DialogRenameFile extends DialogPrompt {
  constructor(resources, initialValue, callback, args) {
    super(resources);
    DESCRIPTIONS = resources.DESCRIPTIONS;
    PLACEHOLDERS = resources.PLACEHOLDERS;
    this.updateValue = initialValue;
    this.callback = callback;
    this.args = args;
  }

  onChange(event) {
    this.updateValue = event.target.value;
  }

  open() {
    super.open({
      title: DESCRIPTIONS.RENAME_FILE,
      placeholder: PLACEHOLDERS.RENAME_FILE,
      initialValue: this.updateValue,
      type: 'text'
    });
  }

  submit() {
    super.submit();
    document.dispatchEvent(
      new RenameFileEvent(this.updateValue, this.callback, this.args)
    );
  }

  reset() {
    super.reset();
    document.dispatchEvent(new RenameFileEvent(null, null, null));
  }
}
