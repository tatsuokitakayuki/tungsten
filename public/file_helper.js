import '/ace/ext-modelist.js';
import {HtmlHelper} from '/html_helper.js';

export class FileHelper {
  static newHelper() {
    return new FileHelper();
  }

  async fetchTextUrlA(url, encodingName) {
    const response = await fetch(url);
    const arrayBuffer = await response.arrayBuffer();
    return this.decodeBuffer(arrayBuffer, encodingName);
  }

  async fetchTextFileA(file, encodingName) {
    const url = URL.createObjectURL(file);
    const text = await this.fetchTextUrlA(url, encodingName);
    URL.revokeObjectURL(url);
    return text;
  }

  downloadBlob(blob, name) {
    const url = URL.createObjectURL(blob);
    const htmlHelper = HtmlHelper.newHelper();
    const a = htmlHelper.a(
      null,
      [
        {name: 'href', value: url},
        {name: 'download', value: name},
        {name: 'rel', value: 'nofollow noreferrer'},
        {name: 'target', value: '_blank'},
        {name: 'type', value: blob.type}
      ]
    );
    a.click();
    URL.revokeObjectURL(url);
  }

  buildBlob(data, type) {
    return new Blob([data], {type: type});
  }

  sanitizeName(name) {
    return name.replace(/[<>"'&|:*?/\\]/gi, ' ').trim();
  }

  hasFSAccess() {
    return 'showOpenFilePicker' in window;
  }

  async verifyPermissionA(fileHandle, readWrite) {
    try {
      let options = {};
      if (readWrite) {
        options = {mode: 'readwrite'};
      }
      if ((await fileHandle.queryPermission(options)) === 'granted') {
        return true;
      }
      if ((await fileHandle.requestPermission(options)) === 'granted') {
        return true;
      }
    } catch (error) {
      console.error(error.name, error.message);
    }
    return false;
  }

  async showOpenFilePickerA(options) {
    try {
      return await window.showOpenFilePicker(options);
    } catch (error) {
      if (error.name !== 'AbortError') {
        console.error(error.name, error.message);
      }
      return null;
    }
  }

  async showSaveFilePickerA(options) {
    try {
      return await window.showSaveFilePicker(options);
    } catch (error) {
      if (error.name !== 'AbortError') {
        console.error(error.name, error.message);
      }
      return null;
    }
  }

  async writeBlobA(blob, fileHandle) {
    const writable = await fileHandle.createWritable();
    await writable.write(blob);
    await writable.close();
  }

  async readTextFileA(file, encodingName) {
    const arrayBuffer = await file.arrayBuffer();
    return this.decodeBuffer(arrayBuffer, encodingName);
  }

  decodeBuffer(arrayBuffer, encodingName) {
    if (Array.isArray(encodingName)) {
      for (const name of encodingName) {
        const decoder = new TextDecoder(name);
        try {
          const decodedString = decoder.decode(arrayBuffer);
          if (!decodedString.includes('\ufffd')) {
            return decodedString;
          }
          console.log(name, 'is FATAL');
        } catch (error) {
          console.error(error.name, error.message);
        }
      }
      return new TextDecoder(encodingName[0]).decode(arrayBuffer);
    }
    return new TextDecoder(encodingName).decode(arrayBuffer);
  }
}
