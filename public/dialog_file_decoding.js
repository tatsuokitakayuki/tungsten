import {ChangeEditorOptionEvent} from '/change_editor_option_event.js';
import {DialogSelect} from '/dialog/dialog_select.js';

let DESCRIPTIONS = null;
let ENCODING_NAMES = null;

export class DialogFileDecoding extends DialogSelect {
  constructor(resources, initialValue) {
    super(resources);
    DESCRIPTIONS = resources.DESCRIPTIONS;
    ENCODING_NAMES = resources.ENCODING_NAMES;
    this.initialValue = initialValue;
  }

  onChange(event) {
    document.dispatchEvent(
      new ChangeEditorOptionEvent('fileDecoding', event.target.value)
    );
  }

  open() {
    super.open(DESCRIPTIONS.FILE_DECODING, ENCODING_NAMES);
  }

  reset() {
    super.reset();
    document.dispatchEvent(
      new ChangeEditorOptionEvent('fileDecoding', this.initialValue)
    );
  }
}
