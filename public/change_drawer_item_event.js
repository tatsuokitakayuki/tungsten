export class ChangeDrawerItemEvent extends CustomEvent {
  constructor(from, to) {
    super('Drawer:changeitem', {detail: {from: from, to: to}});
  }
}
