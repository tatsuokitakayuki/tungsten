import {AppBar} from '/app_bar.js';
import {AppView} from '/app_view.js';
import {ChangeDrawerEvent} from '/change_drawer_event.js';
import {ChangeSnackbarEvent} from '/change_snackbar_event.js';
import {ChangeViewEvent} from '/change_view_event.js';
import {Drawer} from '/drawer.js';
import {FileHelper} from '/file_helper.js';
import {FileManager} from '/file_manager.js';
import {FocusEditorEvent} from '/focus_editor_event.js';
import {HtmlHelper} from '/html_helper.js';
import {Keybinding} from '/keybinding.js';
import {Options} from '/options.js';
import {RenameFileEvent} from '/rename_file_event.js';
import {Resources} from '/resources.js';
import {SaveOptionsEvent} from '/save_options_event.js';
import {Snackbar} from '/snackbar.js';
import {Statusbar} from '/statusbar.js';

let BUTTONS = null;
let DESCRIPTIONS = null;
let EDITOR_OPTIONS = null;
let ENCODING_NAMES = null;
let FILE_URL = null;
let FILES = null;
let NEW_LINE_MODE = null;
let PLACEHOLDERS = null;
let REPOSITORY = null;
let STRINGS = null;

export class Core {
  constructor() {
    this.windowStyle = 'browser';
    if (navigator.standalone === true) {
      this.windowStyle = 'standalone-ios';
    } else if (matchMedia('(display-mode: standalone)').matches === true) {
      this.windowStyle = 'standalone';
    }
    this.editor = ace.edit('editor');
    this.appView = new AppView(this);
    this.appBar = new AppBar();
    this.drawer = new Drawer(this);
    this.options = new Options();
    this.snackbar = new Snackbar();
    this.statusbar = new Statusbar();
    this.fileManager = new FileManager();
    this.keybinding = new Keybinding(this);
    this.dialogModule = null;
    this.fileHelper = FileHelper.newHelper();
  }

  async initializeA() {
    await this.initializeResourcesA();
    this.appView.initialize();
    this.fileManager.initialize(
      {FILE_URL: FILE_URL, FILES: FILES, STRINGS: STRINGS}
    );
    await this.options.initializeA(
      this.getEditor(),
      this.getEditorSession(),
      {EDITOR_OPTIONS: EDITOR_OPTIONS, STRINGS: STRINGS}
    );
    this.keybinding.initialize(
      this.getEditor(),
      {
        DESCRIPTIONS: DESCRIPTIONS,
        FILES: FILES,
        REPOSITORY: REPOSITORY
      }
    );
    this.appBar.initialize();
    this.drawer.initialize();
    this.snackbar.initialize();
    this.statusbar.initialize({NEW_LINE_MODE: NEW_LINE_MODE});
    this.initializeListeners();
    this.setWindowTitle();
    await this.newFileA();
    await this.welcomeAppA();
    document.dispatchEvent(new FocusEditorEvent());
    document.dispatchEvent(
      new ChangeViewEvent({from: 0, to: 0}, {all: true})
    );
    if (this.getOption('drawerOpened', true)) {
      document.dispatchEvent(new ChangeDrawerEvent());
    }
    this.getEditor().resize();
    this.launchQueue();
  }

  initializeListeners() {
    this.getEditor().on('focus', () => this.onEditorFocus());
    this.getEditor().on(
      'changeSelection', () => this.onEditorChangeSelection()
    );
    this.getEditor().on(
      'keyboardActivity', () => this.onEditorKeyboardActivity()
    );
    const htmlHelper = HtmlHelper.newHelper();
    htmlHelper.listen(
      'beforeunload', event => this.onBeforeUnload(event), {}, window
    );
    htmlHelper.listen(
      'Core:focuseditor', () => this.onFocus(), {passive: true}, document
    );
    htmlHelper.listen(
      'Editor:changeoption',
      event => this.onChangeOption(event),
      {passive: true},
      document
    );
    htmlHelper.listen(
      'dragover', event => this.onDragOver(event), false, document
    );
    htmlHelper.listen(
      'drop', async event => await this.onDropA(event), false, document
    );
    htmlHelper.listen(
      'File:save',
      async () => await this.onSaveFileA(),
      {passive: true},
      document
    );
    htmlHelper.listen(
      'Editor:commandpalette',
      () => this.onOpenCommandPalette(),
      {passive: true},
      document
    );
  }

  async initializeResourcesA() {
    const resources = new Resources();
    BUTTONS = await resources.getJsonA('buttons.json');
    DESCRIPTIONS = await resources.getJsonA('descriptions.json');
    EDITOR_OPTIONS = await resources.getJsonA('editor_options.json');
    ENCODING_NAMES = await resources.getJsonA('encoding_names.json');
    FILE_URL = await resources.getJsonA('file_url.json');
    FILES = await resources.getJsonA('files.json');
    NEW_LINE_MODE = await resources.getJsonA('new_line_mode.json');
    PLACEHOLDERS = await resources.getJsonA('placeholders.json');
    REPOSITORY = await resources.getJsonA('repository.json');
    STRINGS = await resources.getJsonA('strings.json');
  }

  onBeforeUnload(event) {
    if (!this.isSafe()) {
      event.preventDefault();
      event.returnValue = '';
    }
  }

  onDragOver(event) {
    event.preventDefault();
  }

  async onDropA(event) {
    event.preventDefault();
    if (this.fileHelper.hasFSAccess()) {
      for (const item of event.dataTransfer.items) {
        if (item.kind === 'file') {
          const entry = await item.getAsFileSystemHandle();
          if (entry.kind === 'file') {
            await this.openHandleA([entry]);
          }
        }
      }
      return;
    }
    const {files} = event.dataTransfer;
    if (files) {
      await this.openFileA(files);
    }
  }

  onEditorFocus() {
    const active = this.getActive();
    if (!this.isCoreFile(active)) {
      this.setReadOnly(active, this.getEditor().getOption('readOnly'));
    }
    document.dispatchEvent(
      new ChangeViewEvent({from: active, to: active}, {all: true})
    );
    document.dispatchEvent(
      new SaveOptionsEvent(
        this.getEditor(),
        this.getEditorSession(),
        {editor: true, extensions: true, app: true}
      )
    );
  }

  onEditorChangeSelection() {
    this.updateStatusDisplay();
  }

  onEditorKeyboardActivity() {
    this.updateStatusDisplay();
  }

  onChangeOption(event) {
    let {name, value} = event.detail;
    switch (name) {
      case 'drawerOpened':
        this.setAppOption(name, value);
        document.dispatchEvent(
          new SaveOptionsEvent(
            this.getEditor(), this.getEditorSession(), {app: true}
          )
        );
        break;
      case 'fileDecoding':
        this.setAppOption(name, value);
        break;
      default:
        this.getEditor().setOption(name, value);
        break;
    }
  }

  onFocus() {
    this.getEditor().focus();
  }

  async onSaveFileA() {
    await this.saveFileA(this.getActive());
  }

  onOpenCommandPalette() {
    this.getEditor().execCommand('openCommandPallete');
  }

  getEditor() {
    return this.editor;
  }

  getEditorSession() {
    return this.getEditor().session;
  }

  getOption(name) {
    let optionValue = '';
    switch (name) {
      case 'drawerOpened':
      case 'fileDecoding':
      case 'welcome':
        optionValue = this.getAppOption(name);
        break;
      default:
        optionValue = this.getEditor().getOption(name);
        break;
    }
    return optionValue;
  }

  getAppOption(name) {
    return Object.getOwnPropertyDescriptor(
      this.options.getAppOptions(), name
    ).value;
  }

  getWindowStyle() {
    return this.windowStyle;
  }

  getActive() {
    return this.fileManager.getActive();
  }

  getDisplayName(index) {
    return this.fileManager.getDisplayName(index);
  }

  getEditSession(index) {
    return this.fileManager.getEditSession(index);
  }

  getFileData(index) {
    return this.fileManager.getFileData(index);
  }

  getFileHandle(index) {
    return this.fileManager.getFileHandle(index);
  }

  getLength() {
    return this.fileManager.list.length;
  }

  getName(index) {
    return this.fileManager.getName(index);
  }

  getTime(index) {
    return this.fileManager.getTime(index);
  }

  getUrl(index) {
    return this.fileManager.getUrl(index);
  }

  findIndex(href) {
    return this.fileManager.findIndex(href);
  }

  isClean(index) {
    return this.fileManager.isClean(index);
  }

  isCoreFile(index) {
    return this.fileManager.isCoreFile(index);
  }

  isEmptyFile(index) {
    return this.fileManager.isEmptyFile(index);
  }

  isReadOnly(index) {
    return this.fileManager.isReadOnly(index);
  }

  isSafe() {
    return this.fileManager.list.every(item => item.isClean());
  }

  hasUndo(index) {
    return this.fileManager.hasUndo(index);
  }

  getDrawer() {
    return this.drawer.drawer;
  }

  setAppOption(name, value) {
    Object.defineProperty(
      this.options.getAppOptions(),
      name,
      {
        configurable: true,
        enumerable: true,
        value: value,
        writable: true,
      }
    );
  }

  setEditorSession(editSession) {
    this.getEditor().setSession(editSession);
  }

  setActive(index) {
    this.fileManager.setActive(index);
  }

  setDisplayName(index, displayName) {
    this.fileManager.setDisplayName(index, displayName);
  }

  setEditSession(index, editSession) {
    this.fileManager.setEditSession(index, editSession);
  }

  setFileHandle(index, fileHandle) {
    this.fileManager.setFileHandle(index, fileHandle);
  }

  setName(index, name) {
    this.fileManager.setName(index, name);
  }

  setReadOnly(index, readOnly) {
    this.fileManager.setReadOnly(index, readOnly);
  }

  setUntitledName(index) {
    this.fileManager.setUntitledName(index);
  }

  setUrl(index, url) {
    this.fileManager.setUrl(index, url);
  }

  dropFileData(index) {
    this.fileManager.dropFileData(index);
  }

  markClean(index) {
    this.fileManager.markClean(index);
  }

  async openUrlA(index, url) {
    return await this.fileManager.openUrlA(index, url);
  }

  async welcomeAppA() {
    if (this.getOption('welcome')) {
      return;
    }
    await this.aboutA();
    this.setAppOption('welcome', true);
    document.dispatchEvent(
      new SaveOptionsEvent(
        this.getEditor(), this.getEditorSession(), {app: true}
      )
    );
  }

  setWindowTitle() {
    const host = location.host;
    document.title = `${STRINGS.APP_NAME} [${host}]`;
  }

  createFileData() {
    return this.fileManager.createFileData();
  }

  async newFileA() {
    const from = this.getActive();
    if (from >= 0) {
      this.setEditSession(from, this.getEditorSession());
    }
    const to = this.createFileData();
    await this.options.initializeSessionOptionsA(this.getEditSession(to));
    this.setActive(to);
    this.setUntitledName(to);
    this.setEditorSession(this.getEditSession(to));
    if (!this.drawer.hasItem(to)) {
      this.drawer.addItem(to);
    }
    document.dispatchEvent(
      new ChangeViewEvent({from: from, to: to}, {all: true})
    );
  }

  async openFileA(files) {
    const fileDecoding = this.getDecodeList();
    const from = this.getActive();
    this.setEditSession(from, this.getEditorSession());
    let to = from;
    for (let i = 0; i < files.length; i++) {
      to = this.createFileData();
      await this.options.initializeSessionOptionsA(this.getEditSession(to));
      const result = await this.fileManager.openFileA(
        to, files[i], fileDecoding
      );
      if (result.success) {
        if (from != to) {
          this.drawer.addItem(to);
        }
      } else {
        if (this.getLength() >= 2) {
          this.dropFileData(to);
        }
        to = from;
      }
    }
    this.setActive(to);
    document.dispatchEvent(
      new ChangeViewEvent({from: from, to: to}, {all: true})
    );
    document.dispatchEvent(new FocusEditorEvent());
  }

  async openHandleA(fileHandles) {
    const fileDecoding = this.getDecodeList();
    const from = this.getActive();
    this.setEditSession(from, this.getEditorSession());
    let to = from;
    for (const handle of fileHandles) {
      if (!(await this.fileHelper.verifyPermissionA(handle, false))) {
        continue;
      }
      const sameFileHandleIndex =
        await this.fileManager.findFileHandleIndexA(handle);
      if (sameFileHandleIndex == -1) {
        to = this.createFileData();
        await this.options.initializeSessionOptionsA(this.getEditSession(to));
        const result = await this.fileManager.openHandleA(
          to, handle, fileDecoding
        );
        if (result.success) {
          if (from != to) {
            this.drawer.addItem(to);
          }
        } else {
          if (this.getLength() >= 2) {
            this.dropFileData(to);
          }
          to = from;
        }
      } else {
        to = sameFileHandleIndex;
      }
    }
    this.setActive(to);
    document.dispatchEvent(
      new ChangeViewEvent({from: from, to: to}, {all: true})
    );
    document.dispatchEvent(new FocusEditorEvent());
  }

  isOpenDialog() {
    return this.dialogModule ? this.dialogModule.isOpen() : false;
  }

  async openSelectFileA() {
    document.dispatchEvent(new FocusEditorEvent());
    if (this.fileHelper.hasFSAccess()) {
      const options = {multiple: true};
      const fileHandles = await this.fileHelper.showOpenFilePickerA(options);
      if (fileHandles) {
        await this.openHandleA(fileHandles);
      }
    } else {
      const {DialogSelectFile} = await import('/dialog_select_file.js');
      const dialogSelectFile = new DialogSelectFile();
      const files = await dialogSelectFile.openA(true);
      if (files) {
        await this.openFileA(files);
      }
    }
  }

  async saveFileA(index) {
    if (!this.fileHelper.hasFSAccess()) {
      await this.downloadFileA(index);
      return;
    }
    const handle = this.getFileHandle(index);
    if (!handle) {
      await this.renameFileA(
        index, () => this.getEditor().execCommand('appSaveFile'), {}
      );
      return;
    }
    if (!(await this.fileHelper.verifyPermissionA(handle, true))) {
      return;
    }
    const result = await this.fileManager.saveFileA(index);
    if (!result) {
      document.dispatchEvent(
        new ChangeSnackbarEvent(STRINGS.NOT_SAVE_FILE, true, null)
      );
      return;
    }
    document.dispatchEvent(
      new ChangeSnackbarEvent(
        STRINGS.SAVE_FILE.replace('$filename$', this.getName(index)), true, null
      )
    );
    this.markClean(index);
    document.dispatchEvent(
      new ChangeViewEvent({from: index, to: index}, {all: true})
    );
  }

  async downloadFileA(index) {
    const url = this.getUrl(index);
    if (url.pathname.includes('/' + FILE_URL.DIRS.INTERNAL + '/')) {
      await this.renameFileA(
        index, () => this.getEditor().execCommand('appSaveFile'), {}
      );
      return;
    }
    await this.fileManager.downloadFile(index);
    document.dispatchEvent(
      new ChangeSnackbarEvent(
        STRINGS.DOWNLOAD_FILE.replace('$filename$', this.getName(index)),
        true,
        null
      )
    );
    this.markClean(index);
    document.dispatchEvent(
      new ChangeViewEvent({from: index, to: index}, {all: true})
    );
  }

  async renameFileA(index, callback, args) {
    if (this.isCoreFile(index)) {
      return;
    }
    this.fileManager.addEventListener();
    if (this.fileHelper.hasFSAccess()) {
      const options = {
        suggestedName: this.getName(index),
        types: await new Resources().getJsonA('file_types.json')
      };
      const handle = await this.fileHelper.showSaveFilePickerA(options);
      if (handle) {
        this.setFileHandle(index, handle);
        const name = (await handle.getFile()).name;
        document.dispatchEvent(
          new RenameFileEvent(name, callback, args)
        );
      }
      document.dispatchEvent(
        new ChangeViewEvent({from: index, to: index}, {all: true})
      );
      document.dispatchEvent(new FocusEditorEvent());
      return;
    }
    const {DialogRenameFile} = await import('/dialog_rename_file.js');
    const dialogRenameFile = new DialogRenameFile(
      {
        BUTTONS: BUTTONS,
        DESCRIPTIONS: DESCRIPTIONS,
        PLACEHOLDERS: PLACEHOLDERS
      },
      this.getName(index), callback, args
    );
    dialogRenameFile.open();
  }

  async closeFileA(index) {
    if (!this.isCoreFile(index) && !this.isClean(index)) {
      if (this.isOpenDialog()) {
        return;
      }
      const {DialogConfirm} = await import('/dialog/dialog_confirm.js');
      const dialogConfirm = new DialogConfirm({BUTTONS: BUTTONS});
      this.dialogModule = dialogConfirm;
      dialogConfirm.open(
        DESCRIPTIONS.CLOSE_FILE,
        STRINGS.CONFIRM_CLOSE_FILE
            .replace('$filename$', this.getName(index)),
        async args => await this.closeFileCallbackA(args),
        {index: index}
      );
    } else {
      await this.closeFileCallbackA({index: index});
    }
  }

  async closeFileCallbackA(args) {
    this.dropFileData(args.index);
    this.drawer.removeItem(args.index);
    this.setActive(args.index);
    if (this.getLength() == 0) {
      await this.newFileA();
    }
    const active = this.getActive();
    document.dispatchEvent(
      new ChangeViewEvent({from: args.index, to: active}, {all: true})
    );
    document.dispatchEvent(new FocusEditorEvent());
  }

  createUrl(exists, name) {
    return this.fileManager.createUrl(exists, name);
  }

  async openCoreFileA(coreName) {
    const from = this.getActive();
    let to = from;
    const coreFileUrl = this.createUrl(false, coreName);
    to = this.findIndex(coreFileUrl.href);
    if (to == -1) {
      to = this.createFileData();
      await this.options.initializeSessionOptionsA(this.getEditSession(to));
      const filename = coreName + '.md';
      const url = new URL('res/' + filename, location.href);
      const result = await this.openUrlA(to, url);
      if (result.success) {
        this.setDisplayName(to, coreName);
        this.setReadOnly(to, true);
        this.setUrl(to, coreFileUrl);
        if (!this.drawer.hasItem(to)) {
          this.drawer.addItem(to);
        }
      } else {
        if (this.getLength() >= 2) {
          this.dropFileData(to);
        }
        to = from;
      }
    }
    this.setEditSession(from, this.getEditorSession());
    this.setActive(to);
    document.dispatchEvent(
      new ChangeViewEvent({from: from, to: to}, {all: true})
    );
  }

  async cacheListA() {
    await this.openCoreFileA(FILES.CACHE_LIST);
  }

  async aboutA() {
    await this.openCoreFileA(FILES.ABOUT);
  }

  flipReadOnly(index) {
    const readOnly = !this.isReadOnly(index);
    this.setReadOnly(index, readOnly);
    return readOnly;
  }

  idToName(id) {
    return id.replace(
      /-./gi, match => match.slice(1).toUpperCase()
    ).replace('Ime', 'IME');
  }

  async exportOptionsA(editor, editSession) {
    await this.options.exportOptionsA(editor, editSession);
    document.dispatchEvent(
      new ChangeSnackbarEvent(STRINGS.EXPORTED_OPTIONS, true, null)
    );
  }

  async importOptionsA(editor, editSession) {
    document.dispatchEvent(new FocusEditorEvent());
    const {DialogSelectFile} = await import('/dialog_select_file.js');
    const dialogSelectFile = new DialogSelectFile();
    const files = await dialogSelectFile.openA(false);
    await this.options.importOptionsA(files[0], editor, editSession);
    document.dispatchEvent(
      new SaveOptionsEvent(editor, editSession, {all: true})
    );
    const active = this.getActive();
    document.dispatchEvent(
      new ChangeViewEvent({from: active, to: active}, {all: true})
    );
    document.dispatchEvent(
      new ChangeSnackbarEvent(STRINGS.IMPORTED_OPTIONS, true, null)
    );
  }

  updateStatusDisplay() {
    const index = this.getActive();
    if (index < 0) {
      return;
    }
    document.dispatchEvent(
      new ChangeViewEvent(
        {from: index, to: index},
        {draweritem: true, appbar: true, statusbar: true}
      )
    );
  }

  async fileDecodingA() {
    if (this.isOpenDialog()) {
      return;
    }
    const {DialogFileDecoding} = await import(
      '/dialog_file_decoding.js'
    );
    const dialog = new DialogFileDecoding(
      {
        BUTTONS: BUTTONS,
        DESCRIPTIONS: DESCRIPTIONS,
        ENCODING_NAMES: ENCODING_NAMES
      },
      this.getOption('fileDecoding')
    );
    dialog.open();
    this.dialogModule = dialog;
  }

  getDecodeList() {
    const list = [...ENCODING_NAMES];
    const value = this.getOption('fileDecoding');
    if (value != ENCODING_NAMES[0].VALUE) {
      const index = list.findIndex(item => item.VALUE == value);
      let array = list.splice(index, 1);
      const group = array[0].GROUP;
      if (group) {
        let index = list.findIndex(item => item.GROUP == group);
        while (index != -1) {
          array = array.concat(list.splice(index, 1));
          index = list.findIndex(item => item.GROUP == group);
        }
      }
      list.splice(1, 0, ...array);
    }
    return list.map(item => item.VALUE);
  }

  launchQueue() {
    if ('launchQueue' in window) {
      launchQueue.setConsumer(async launchParams => {
        // Nothing to do when the queue is empty.
        if (!launchParams.files.length) {
          return;
        }
        const fileHandles = [];
        for (const fileHandle of launchParams.files) {
          fileHandles.push(fileHandle);
        }
        await this.openHandleA(fileHandles);
      });
    }
  }
}
