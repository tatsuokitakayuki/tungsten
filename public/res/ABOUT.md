# <🖊> Tungsten Text Editor

* Close this file with `Alt+W (Opt+W)`.
* Open command palette with `F1`.
* Show keyboard shortcuts with `Ctrl+Alt+H (Cmd+Opt+H)`.

## What is this?

Tungsten Text Editor is a small, lightweight notepad-like text editor.

We are developing Tungsten Text Editor mainly for Chromebook (or ChromiumOS and
its based OS) users.

In addition, the results of Web Capabilities (Project Fugu 🐡) [^1] are used to
implement features that are not feasible for general web applications, such as
local file access functions.

[^1]: https://www.chromium.org/teams/web-capabilities-fugu/

## Information

Copyright (c) 2020 龍興 尚幸 (TATSUOKI Takayuki)

This project is open source and available under the MIT License [^2].

* **Project repository**:
  https://gitlab.com/tatsuokitakayuki/tungsten
* **Open Source Software License**:
  https://gitlab.com/tatsuokitakayuki/tungsten/-/blob/master/OPEN_SOURCE_LICENSE.md
* **Stable channel**:
  https://pwatungsten.app/
* **Google Play Store (stable)**:
  https://play.google.com/store/apps/details?id=app.kirari.tungsten.twa
* **Dev channel (UNSTABLE!)**:
  https://dev.pwatungsten.app/
* **𝕏**: **@pwatungsten**:
  https://x.com/pwatungsten
* **Discord**:
  https://discord.gg/rwxR2ncG

[^2]: https://gitlab.com/tatsuokitakayuki/tungsten/-/blob/master/LICENSE

## Please support this project!🙏

* **Support on Patreon**:
  https://www.patreon.com/TatsuokiTakayuki
* **Support on pixivFANBOX**:
  https://tatsuokitakayuki.fanbox.cc/
* **Product sales site (Kiraristar.com Product Sales Department)**:
  https://kiraristar.booth.pm/

## Authors

* Maintainer, Developer: 龍興 尚幸 (TATSUOKI Takayuki)
  - **𝕏**: **@ttatsuoki**:
    https://x.com/ttatsuoki
  - **Email**: takayuki.tatsuoki@gmail.com
