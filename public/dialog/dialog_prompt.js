import {Dialog} from '/dialog/dialog.js';
import {HtmlHelper} from '/html_helper.js';
import {MaterialHelper} from '/material_helper.js';

export class DialogPrompt extends Dialog {
  buildContent(options) {
    const htmlHelper = HtmlHelper.newHelper();
    if (options.message) {
      this.dialogContent.appendChild(htmlHelper.p(options.message));
    }
    const attributes = [
      {name: 'id', value: 'dialog-input-field'},
      {name: 'type', value: options.type},
      {name: 'value', value: options.initialValue},
      {name: 'aria-label', value: 'label'}
    ];
    if (options.placeholder) {
      attributes.push({name: 'placeholder', value: options.placeholder});
    }
    if (options.type == 'number') {
      if (options.min !== null) {
        attributes.push({name: 'min', value: options.min});
      }
      if (options.max !== null) {
        attributes.push({name: 'max', value: options.max});
      }
    }
    const input = htmlHelper.input('', attributes);
    input.classList.add('mdc-text-field__input');
    input.onchange = event => this.onChange(event);
    const label = htmlHelper.label();
    label.classList.add(
      'mdc-text-field',
      'mdc-text-field--outlined',
      'mdc-text-field--no-label'
    );
    label.appendChild(input);
    const materialHelper = new MaterialHelper();
    label.appendChild(materialHelper.notchedOutline());
    this.dialogContent.appendChild(label);
  }

  open(options) {
    this.buildTitle(options.title);
    this.buildContent(options);
    this.buildFooter();
    this.setListeners();
    this.dialog.open();
  }
}
