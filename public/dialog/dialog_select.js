import {Dialog} from '/dialog/dialog.js';
import {HtmlHelper} from '/html_helper.js';
import {MaterialHelper} from '/material_helper.js';

export class DialogSelect extends Dialog {
  buildSelectList(items) {
    const htmlHelper = HtmlHelper.newHelper();
    const selectList = [];
    items.forEach(item => {
      const option = htmlHelper.option(
        String(item.NAME), [{name: 'value', value: String(item.VALUE)}]
      );
      option.classList.add('mdc-list-item');
      if (item.VALUE == this.initialValue) {
        option.classList.add('mdc-list-item--selected');
        option.setAttribute('selected', '');
      }
      selectList.push(option);
    });
    return selectList;
  }

  buildContent(selectData) {
    if (!selectData) {
      return;
    }
    const htmlHelper = HtmlHelper.newHelper();
    const select = htmlHelper.select(
      '', [{name: 'id', value: 'dialog-select-field'}]
    );
    select.classList.add('mdc-select__native-control');
    this.buildSelectList(selectData).forEach(
      item => select.appendChild(item)
    );
    select.onchange = event => this.onChange(event);
    const div = htmlHelper.div();
    div.classList.add('mdc-select','mdc-select--no-label');
    div.appendChild(select);
    const materialHelper = new MaterialHelper();
    div.appendChild(materialHelper.underline());
    this.dialogContent.appendChild(div);
  }

  open(title, selectData) {
    this.buildTitle(title);
    this.buildContent(selectData);
    this.buildFooter();
    this.setListeners();
    this.dialog.open();
  }
}
