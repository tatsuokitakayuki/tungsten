import {Dialog} from '/dialog/dialog.js';
import {HtmlHelper} from '/html_helper.js';

export class DialogConfirm extends Dialog {
  constructor(resources) {
    super(resources);
    this.callback = null;
    this.args = null;
  }

  buildContent(message) {
    if (!message) {
      return;
    }
    const htmlHelper = HtmlHelper.newHelper();
    this.dialogContent.appendChild(htmlHelper.p(message));
  }

  open(title, message, callback, args) {
    this.callback = callback;
    this.args = args;
    this.buildTitle(title);
    this.buildContent(message);
    this.buildFooter();
    this.setListeners();
    this.dialog.open();
  }

  submit() {
    super.submit();
    if (this.callback) {
      this.callback(this.args);
    }
  }
}
