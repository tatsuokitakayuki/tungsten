export class SaveOptionsEvent extends CustomEvent {
  constructor(editor, editSession, options) {
    super(
      'Options:save',
      {detail: {editor: editor, editSession: editSession, options: options}}
    );
  }
}
