import '/ace/ext-modelist.js';
import {HtmlHelper} from '/html_helper.js';

const ModeList = ace.require('ace/ext/modelist');

let NEW_LINE_MODE = null;

export class StatusHelper {
  constructor(resources) {
    NEW_LINE_MODE = resources.NEW_LINE_MODE;
    this.readOnly = 'Read-only';
    this.editable = 'Editable';
    this.modified = '*';
    this.recording = '(Rec)';
    this.overwrite = 'Overwrite';
    this.insert = 'Insert';
  }

  buildStatusText(displayName, editor, fileData) {
    const htmlHelper = HtmlHelper.newHelper();
    const items = [];
    items.push(this.buildDisplayNameText(displayName, fileData.editSession));
    items.push(this.buildInputModeText(editor));
    items.push(this.buildRecordingText(editor));
    items.push(this.buildCursorPositionText(editor));
    items.push(this.buildSelectionText(editor));
    items.push(this.buildLineText(fileData.editSession));
    items.push(this.buildCharacterText(fileData.editSession));
    items.push(this.buildReadOnlyText(fileData.readOnly));
    items.push(this.buildLanguageModeText(fileData.editSession));
    items.push(this.buildNewLineModeText(fileData.editSession));
    items.push(this.buildTabSizeText(fileData.editSession));
    items.push(this.buildOverwriteText(fileData.editSession));
    return items.filter(item => Boolean(item))
        .map(item => htmlHelper.span(item));
  }

  buildInputModeText(editor) {
    let inputMode = editor.keyBinding.getStatusText(editor);
    if (inputMode) {
      return inputMode;
    }
    return '';
  }

  buildRecordingText(editor) {
    if (editor.commands.recording) {
      return this.recording;
    }
    return '';
  }

  buildCursorPositionText(editor) {
    const cursorPosition = editor.getCursorPosition();
    cursorPosition.row += Number(editor.getOption('firstLineNumber'));
    cursorPosition.column++;
    return String(cursorPosition.row) + ':' +
        String(cursorPosition.column);
  }

  buildSelectionText(editor) {
    const selection = editor.selection;
    if (selection.isEmpty()) {
      return '';
    }
    const range = editor.getSelectionRange();
    return '(' + String(range.end.row - range.start.row) + ':' +
        String(range.end.column - range.start.column) + ')';
  }

  buildLineText(editSession) {
    const line = editSession.getLength();
    return new Intl.NumberFormat().format(line) + ' ' +
        (line > 1 ? 'lines' : 'line');
  }

  buildCharacterText(editSession) {
    const character = editSession.getValue().length;
    return new Intl.NumberFormat().format(character) + ' ' +
        (character > 1 ? 'characters' : 'character');
  }

  buildReadOnlyText(readOnly) {
    if (readOnly) {
      return this.readOnly;
    }
    return this.editable;
  }

  buildOverwriteText(editSession) {
    if (editSession.getOption('overwrite')) {
      return this.overwrite;
    }
    return this.insert;
  }

  buildLanguageModeText(editSession) {
    const value = editSession.getOption('mode');
    const caption = ModeList.modes
        .find(item => item.mode == value).caption;
    return caption;
  }

  buildNewLineModeText(editSession) {
    return NEW_LINE_MODE
        .find(item => item.VALUE == editSession.getOption('newLineMode'))
        .NAME;
  }

  buildTabSizeText(editSession) {
    const tabSize = editSession.getOption('tabSize');
    return tabSize + ' ' + (tabSize > 1 ? 'spaces' : 'space');
  }

  buildDisplayNameText(displayName, editSession) {
    if (!editSession.getUndoManager().isClean()) {
      return this.modified + displayName;
    }
    return displayName;
  }
}
