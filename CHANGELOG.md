# CHANGELOG

## Version 19.0.0.20250306 (dev)
- Added file extensions that can be opened from the file manager.

## Version 19.0.0.20250305 (dev)
- Organized screenshots.
- Organized file list.
- Updated res/ABOUT.md.
- Added a parameter to the screenshots section of the manifest.

## Version 19.0.0.20250304 (dev)
- Fixed the description of the application.

## Version 18.0.0.20250225 (dev)
- Fixed file_handlers section.

## Version 18.0.0.20250224 (dev)
- Updated for ace-builds version 1.39.0 base.
  - Added CSV and TSV.

## Version 18.0.0.20250217 (dev)
- Updated for ace-builds version 1.38.0 base.

## Version 18.0.0.20250205 (dev)
- Fixed to select JSON mode when opening Webmanifest files.

## Version 18.0.0.20250117 (dev)
- Updated for ace-builds version 1.37.5 base.

## Version 18.0.0.20250113 (dev)
- Organized functions.

## Version 18.0.0.20250110 (dev)
- Updated for ace-builds version 1.37.4 base.

## Version 18.0.0.20250109 (dev)
- Fixed function names.

## Version 18.0.0.20250108 (dev)
- Updated for ace-builds version 1.37.3 base.

## Version 18.0.0.20250107 (dev)
- Updated for ace-builds version 1.37.2 base.

## Version 18.0.0.20241223 (dev)
- Updated for ace-builds version 1.37.1 base.

## Version 18.0.0.20241219 (dev)
- Fixed meta tag.

## Version 18.0.0.20241218 (dev)
- Updated for ace-builds version 1.37.0 base.

## Version 17.0.0.20241118 (dev)
- Updated for ace-builds version 1.36.5 base.

## Version 17.0.0.20241105 (dev)
- Updated for ace-builds version 1.36.4 base.

## Version 17.0.0.20241022 (dev)
- Updated for ace-builds version 1.36.3 base.

## Version 17.0.0.20241021 (dev)
- Updated res/ABOUT.md.
- Changed some screenshots.
- Fixed the file list of the application.

## Version 17.0.0.20240910 (dev)
- Organized keyboard shortcuts.

## Version 17.0.0.20240902 (dev)
- Updated for ace-builds version 1.36.2 base.

## Version 16.0.0.20240821 (dev)
- Updated for ace-builds version 1.35.5 base.

## Version 16.0.0.20240729 (dev)
- Updated res/ABOUT.md.

## Version 16.0.0.20240723 (dev)
- Fixed ABOUT to make it easier to check in the editor.
- Updated for ace-builds version 1.35.4 base.

## Version 16.0.0.20240719 (dev)
- Updated for ace-builds version 1.35.3 base.

## Version 16.0.0.20240702 (dev)
- Updated for ace-builds version 1.35.2 base.

## Version 16.0.0.20240701 (dev)
- Updated for ace-builds version 1.35.1 base.

## Version 16.0.0.20240612 (dev)
- Updated for ace-builds version 1.35.0 base.

## Version 15.0.0.20240530 (dev)
- Updated for ace-builds version 1.34.2 base.

## Version 15.0.0.20240523 (dev)
- Updated for ace-builds version 1.34.0 base.

## Version 15.0.0.20240522 (dev)
- Updated for ace-builds version 1.33.3 base.

## Version 15.0.0.20240521 (dev)
- Updated for ace-builds version 1.33.2 base.

## Version 15.0.0.20240509 (dev)
- Fixed the file listing for ace-builds.

## Version 15.0.0.20240508 (dev)
- Updated for ace-editor-extensions version 2.1.0 base.

## Version 15.0.0.20240425 (dev)
- Updated res/ABOUT.md.

## Version 15.0.0.20240424 (dev)
- Updated for ace-builds version 1.33.1 base.

## Version 14.0.0.20240214 (dev)
- Updated for ace-builds version 1.32.6 base.

## Version 14.0.0.20231205 (dev)
- Updated for ace-builds version 1.32.2 base.

## Version 14.0.0.20231204 (dev)
- Updated for ace-builds version 1.32.0 base.

## Version 13.0.0.20231116 (dev)
- Updated for ace-builds version 1.31.2 base.

## Version 13.0.0.20231101 (dev)
- Updated for ace-builds version 1.31.1 base.

## Version 13.0.0.20231010 (dev)
- Updated for ace-builds version 1.29.0 base.

## Version 13.0.0.20230925 (dev)
- Updated for ace-builds version 1.28.0 base.

## Version 13.0.0.20230911 (dev)
- Updated for ace-builds version 1.24.2 base.

## Version 12.0.0.20230823 (dev)
- Added display_override to manifest.

## Version 12.0.0.20230822 (dev)
- Fixed file_handlers in manifest.
- Added launch_handler to manifest.
- Added id to manifest.

## Version 12.0.0.20230818 (dev)
- Changed "Twitter" to "𝕏" in res/ABOUT.md.

## Version 12.0.0.20230816 (dev)
- Updated for ace-builds version 1.24.1 base.

## Version 12.0.0.20230717 (dev)
- Updated manifest.webmanifest.
- Updated res/ABOUT.md.

## Version 12.0.0.20230711 (dev)
- Updated for ace-builds version 1.23.3 base.

## Version 12.0.0.20230703 (dev)
- Updated for ace-builds version 1.23.1 base.

## Version 12.0.0.20230629 (dev)
- Removed unnecessary conditionals.

## Version 12.0.0.20230612 (dev)
- Changed the specification of ChangeViewEvent.
- Updated for ace-builds version 1.22.1 base.
- Fixed confusing variable names.

## Version 12.0.0.20230609 (dev)
- Removed unnecessary async and await.

## Version 12.0.0.20230605 (dev)
- Fixed to not focus on AppBar buttons.

## Version 12.0.0.20230531 (dev)
- Updated for ace-builds version 1.22.0 base.

## Version 11.0.0.20230523 (dev)
- Changed "pallete" to "palette". (except one location)

## Version 11.0.0.20230522 (dev)
- Changed button label definition from aria-label element to title element.

## Version 11.0.0.20230517 (dev)
- Updated res/descriptions.json.
- Updated for ace-builds version 1.21.1 base.

## Version 11.0.0.20230516 (dev)
- Updated res/ABOUT.md.
- Added "Show keyboard shortcuts" command shortcut.
- Updated for ace-builds version 1.21.0 base.

## Version 11.0.0.20230509 (dev)
- Fixed some shortcuts on Mac.

## Version 11.0.0.20230508 (dev)
- Changed "Move lines down/up" shortcut.

## Version 11.0.0.20230502 (dev)
- Fixed AppBar class.

## Version 11.0.0.20230501 (dev)
- Updated version number.

## Version 11.0.0.20230428 (dev)
- Rewritten isOpenDialog.
- Changed onFocus specification.

## Version 11.0.0.20230423 (dev)
- Changed the version notation of Material Design Icons.
- Updated for ace-builds version 1.18.0 base.

## Version 11.0.0.20230418 (dev)
- Updated res/ABOUT.md.

## Version 11.0.0.20230417 (dev)
- Moved strings from core.js to strings.json.

## Version 11.0.0.20230413 (dev)
- Fixed some strings.
- Updated for Material Design Icons version 4.0.0 (2023/03/22) base.
- Updated for ace-builds version 1.17.0 base.
- Removed unnecessary keyboard shortcuts.

## Version 11.0.0.20230410 (dev)
- Organized and fixed res/descriptions.json.
- Fixed a bug in which async functions are called without awaiting.
- Optimized some codes.
- Fixed some keyboard shortcuts.

## Version 11.0.0.20230316 (dev)
- Added enabling the CodeLens extension on app starting.

## Version 11.0.0.20230315 (dev)
- Updated for ace-builds version 1.15.3 base.

## Version 10.0.0.20230220 (dev)
- Added "GROUP" to some items in res/encoding_names.json.

## Version 10.0.0.20230209 (dev)
- Added behavior when the decode option is the default value.
- Changed to decode until a result that seems normal is obtained.

## Version 9.0.1.20221219 (stable)
- Updated app manifest categories from "editor" to "developer tools" and "utilities".

## Version 10.0.0.20221216 (dev)
- Updated for ace-builds version 1.14.0 base.

## Version 10.0.0.20221208 (dev)
- Updated for ace-builds version 1.13.2 base.

## Version 10.0.0.20221207 (dev)
- Added one static function.
- Changed the usage of FileHelper.

## Version 9.0.20221118 (dev)
- Updated for ace-builds version 1.13.1 base.

## Version 9.0.20221114 (dev)
- Updated res/file_types.json.
- Updated for ace-builds version 1.13.0 base.

## Version 9.0.20221103 (dev)
- Updated for ace-builds version 1.12.5 base.
- Rolled back view initialization.

## Version 9.0.20221031 (dev)
- Updated res/ABOUT.md.

## Version 9.0.20221019 (dev)
- Fixed view initialization.
- Updated for ace-builds version 1.12.3 base.
- Fixed service worker.

## Version 9.0.20221018 (dev)
- Rewrote AppBar class.
- Changed the format of HtmlHelper class.

## Version 9.0.20221017 (dev)
- Updated for ace-builds version 1.12.1 base.

## Version 9.0.20221016 (dev)
- Updated for ace-builds version 1.12.0 base.

## Version 9.0.20221013 (dev)
- Changed the app name from "Tungsten" to "Tungsten Text Editor".

## Version 9.0.20221004 (dev)
- Separated Tungsten's own part from ace-builds.
- Updated for ace-builds version 1.11.2 base.

## Version 9.0.20220926 (dev)
- Updated manifest.webmanifest.

## Version 9.0.20220922 (dev)
- Updated for ace-builds version 1.11.0 base.

## Version 9.0.20220916 (dev)
- Changed the app icon.

## Version 9.0.20220914 (dev)
- Removed some unnecessary z-indexes.
- Adjusted mdc-elevation--z.

## Version 9.0.20220912 (dev)
- Updated res/ABOUT.md.

## Version 9.0.20220907 (dev)
- Fixed a mistake in variable names.
- Fixed a bug that app cache version can't be retrieved.
- Changed the data format of res/editor_options.json.
- Updated for ace-builds version 1.10.1 base.
- Added one item to editor_options.json.

## Version 9.0.20220906 (dev)
- Changed the app name from "Tungsten" to "Tungsten Text Editor".
- Fixed the app description.

## Version 9.0.20220905 (dev)
- Added option.
- Removed unnecessary items.
- Changed the wording of some items.

## Version 9.0.20220902 (dev)
- Changed Command palette icon from keyboard to terminal.
- Updated for ace-builds version 1.10.0.

## Version 8.4.1.20220812 (dev)
- Fixed a bug that the MaterialIcons package was not installed properly.

## Version 9.0.20220811 (dev)
- Updated two app manifest files.

## Version 9.0.20220810 (dev)
- Updated public/res/ABOUT.md.

## Version 8.4.20220802 (dev)
- Adjusted the line spacing of the options panel.
- Reduced the font size of the options panel.
- Reduced the text size of the status bar.
- Added package.json and package-lock.json to the repository.

## Version 8.4.20220728 (dev)
- Updated ace-builds to version 1.8.1 base.
- Updated with the latest Material Icons documentation.
- Updated LICENSE.
- Updated .gitignore.
- Removed ace-builds related files from the Tungsten repository.
- Removed emmet-core related files from the Tungsten repository.
- Removed Material Icons related files from Tungsten repository.
- Removed Material Components for Web related files from the Tungsten repository.
- Removed localForage related files from the Tungsten repository.

## Version 8.4.20220601 (dev)
- Updated ace-builds to version 1.5.3.

## Version 8.3.20220513 (dev)
- Updated Material Components for the web to version 14.0.0.

## Version 8.2.20211029 (dev)
- Rewrote some source code.

## Version 8.2.20211026 (dev)
- Rewrote some source code.

## Version 8.2.20211025 (dev)
- Rewrote some source code.

## Version 8.2.20211021 (dev)
- Rewrote some source code.

## Version 8.1.20211014 (dev)
- Updated ace-builds to version 1.4.13.

## Version 8.1.20211008 (dev)
- Removed some unnecessary escapes.

## Version 8.1.20211006 (dev)
- Updated app manifest.

## Version 8.1.20211005 (dev)
- Merge branch 'v8-u1'.

## Version 8.0.20210906 (stable)
- Added missing files to the app's cache list.
- Fixed a bug that Chrome 93 could not save files in some directories.

## Version 8.1.20210929 (dev)
- Updated ABOUT.md.

## Version 8.1.20210928 (dev)
- Rewrote some source code.

## Version 8.1.20210927 (dev)
- Updated Material Components for the web to version 13.0.0.
- Aligned the initial values of some options with the initial values of the library.
- Removed some unnecessary properties.

## Version 8.1.20210921 (dev)
- Added labels to all buttons.

## Version 8.1.20210916 (dev)
- Adjusted the purpose parameter for all app icons.
- Fixed File handling data.

## Version 8.1.20210913 (dev)
- Rewrote some source code.

## Version 8.1.20210912 (dev)
- Extends the functionality of some functions in the HtmlHelper class.

## Version 8.0.20210906 (dev)
- Removed some unnecessary variable definitions.
- Updated screenshots.

## Version 8.0.20210905 (dev)
- Updated screenshots.
- Fixed a mistake in resizing. (cbc2e05f)

## Version 8.0.20210902 (dev)
- Updated ABOUT.md.

## Version 8.0.20210901 (dev)
- Fixed to resize the editor part when opening and closing Drawer.
- Removed the animation when opening and closing the Drawer.
- Fixed the state of Drawer when the application is closed so that it will be inherited at the next startup.
- Removed some unnecessary files.
- Fixed a crash in welcome message display processing.

## Version 8.0.20210831 (dev)
- Removed some unnecessary Dynamic imports.
- Fixed welcome message display processing.

## Version 8.0.20210830 (dev)
- Changed the function name "listener" to "listen".
- Rewrote some source code.

## Version 8.0.20210825 (dev)
- Rewrote some source code.
- Rewrote the event listener addition process.

## Version 8.0.20210825 (dev)
- Fixed JSON data reading process.
- Added one function to HtmlHelper class.
- Added the function of two buttons of Top app bar.

## Version 8.0.20210823 (dev)
- Added the file name to the display item of Statusbar.
- Removed some unnecessary string definitions.
- Added the function of Navigation Drawer button.
- Added 3 buttons to the Top app bar.
- Fixed typo.

## Version 8.0.20210819 (dev)
- Updated localForage to version 1.10.0.
- Removed some unnecessary options..
- Removed some unnecessary files.

## Version 8.0.20210817 (dev)
- Removed some unnecessary files.
- Removed some unnecessary functions.

## Version 8.0.20210816 (dev)
- Removed the Drawer button on the Top app bar.
- Changed to open Drawer when the app starts.
- Removed some unnecessary files.
- Removed some menus.

## Version 8.0.20210813 (dev)
- Removed the filename display of the Top app bar.

## Version 8.0.20210812 (dev)
- Fixed a bug that options were not set when creating a File session.

## Version 8.0.20210811 (dev)
- Fixed the processing of New file.
- Added support for opening Google Apps Script file (.gs).
- Fixed the possibility of crashing when using a new file.
- Removed some menus.
- Updated Material Components for the web to version 12.0.0.

## Version 7.2.20210803 (dev)
- Rewrote some source code.

## Version 7.2.20210715 (dev)
- Rewrote the Drag and Drop process.

## Version 7.2.20210709 (dev)
- Rewrote some source code.

## Version 7.2.20210707 (dev)
- Removed privacy policy.
- Removed some unnecessary tags.

## Version 7.1.20210702 (dev)
- Changed the size of favicon.png.
- Fixed the process when the file could not be opened.
- Fixed typo.

## Version 7.1.20210624 (dev)
- Rewrote some source code.

## Version 7.1.20210619 (dev)
- Rewrote some source code.

## Version 7.1.20210617 (dev)
- Rewrote some source code.

## Version 7.1.20210616 (dev)
- Rewrote some source code.

## Version 7.1.20210615 (dev)
- Rewrote some source code.
- Updated the appearance of the file list.

## Version 7.1.20210614 (dev)
- Updated the appearance of menu items.
- Updated Material Components for the web to version 11.0.0.
- Removed some unnecessary properties.
- Renamed the variable.

## Version 7.1.20210613 (dev)
- Rewrote some source code.

## Version 7.1.20210610 (dev)
- Merge branch 'v7-u1'.

## Version 7.0.20210526 (stable)
- Fixed a bug in the setting initialization process.

## Version 7.1.20210609 (dev)
- Removed unnecessary import.
- Rewrote some source code.

## Version 7.1.20210608 (dev)
- Removed some unnecessary whitespace.

## Version 7.1.20210607 (dev)
- Changed the appearance of the File list.

## Version 7.0.20210526 (dev)
- Fixed a crash while processing the Quit app.
- Fixed the procedure to get the Editor class of Ace Editor.

## Version 7.0.20210525 (dev)
- Fixed a bug that the app did not quit when Quit app was selected from the file menu.
- Added some file types that can be selected with Save file picker.
- Added some file types that can be opened by Tungsten from a file manager.

## Version 7.0.20210524 (dev)
- Fixed a bug that the menu button style was not reflected immediately in Import options.
- Fixed the procedure to get the Editor class of Ace Editor.

## Version 7.0.20210521 (dev)
- Fixed the structure of multilingual files.
- Added the item "Number of lines" to the status bar.
- Rewrote some source code.
- Fixed a bug in the file selection operation in the file list.

## Version 7.0.20210520 (dev)
- Rewrote some source code.

## Version 7.0.20210519 (dev)
- Added Japanese version to App Manifest.
- Added index.html to Tungsten's file list.
- Added some options to Save file picker.
- Rewrote some source code.
- Added a feature that can be opened in Tungsten when a text file is selected from the file manager.

## Version 7.0.20210518 (dev)
- Fixed some internal URL misconfigurations.
- Changed all "window.location" to "location".
- Removed unnecessary import.

## Version 7.0.20210517 (dev)
- Rewrote some source code.

## Version 7.0.20210515 (dev)
- Rewrote some source code.

## Version 7.0.20210514 (dev)
- Rewrote some source code.

## Version 7.0.20210513 (dev)
- Rewrote some source code.
- Adjusted the emmet-core library.

## Version 7.0.20210512 (dev)
- Rewrote some source code.

## Version 7.0.20210511 (dev)
- Fixed the process to get the host string.
- Rewrote some source code.

## Version 7.0.20210509 (dev)
- Rewrote some source code.

## Version 7.0.20210508 (dev)
- Fixed description.

## Version 7.0.20210507 (dev)
- Fixed to preload some files.
- Fixed some file paths.

## Version 6.2.20210501 (dev)
- Added Delete to the Edit menu again.

## Version 6.2.20210430 (dev)
- Changed some initializations.
- Removed Delete from the Edit menu.

## Version 6.2.20210428 (dev)
- Changed some initializations.
- Removed some unnecessary files.

## Version 6.2.20210427 (dev)
- Fixed a bug where Export options crashed.
- Changed some initializations.

## Version 6.2.20210426 (dev)
- Removed some unnecessary files.
- Changed some classes into data files.

## Version 6.2.20210421 (dev)
- Removed some unnecessary files.

## Version 6.2.20210419 (dev)
- Removed some unnecessary files.

## Version 6.2.20210414 (dev)
- Fixed the handling of screenshots.

## Version 6.2.20210413 (dev)
- Removed some unnecessary files.
- Removed some unnecessary processing.
- Changed some classes into data files.
- Fixed the menu initialization process.

## Version 6.2.20210412 (dev)
- Removed some unnecessary functions.

## Version 6.2.20210410 (dev)
- Changed some classes into data files.
- Removed some unnecessary functions.

## Version 6.2.20210409 (dev)
- Removed some unnecessary files.
- Changed some classes into data files.
- Fixed a spelling error.

## Version 6.2.20210407 (dev)
- Renamed manifest.json to manifest.webmanifest.
- Changed some classes into data files.
- Added Resources class.

## Version 6.2.20210405 (dev)
- Changed some classes into data files.

## Version 6.1.20210331 (dev)
- Fixed the size of apple-touch-icon.

## Version 6.1.20210330 (dev)
- Fixed a bug where switching Theme was incomplete.

## Version 6.1.20210328 (dev)
- Updated screenshots.
- Removed the file type selection function of the open file selection dialog.

## Version 6.1.20210326 (dev)
- Added a file type selection function to the open file selection dialog.

## Version 6.1.20210325 (dev)
- Formatted source code.

## Version 6.1.20210324 (dev)
- Fixed the processing of AppOptions.
- Fixed window.navigator to navigator.

## Version 6.1.20210323 (dev)
- Removed some unnecessary processing.
- Rewrote the menu button processing.
- Fixed so that unexpected line breaks are not created.

## Version 6.1.20210316 (dev)
- Removed some unnecessary properties.
- Removed some unnecessary functions.

## Version 6.1.20210312 (dev)
- Rewrote some variable definitions.

## Version 6.0.20210226 (dev)
- Fixed a bug that the status display was not updated when editing with the mouse.

## Version 6.0.20210225 (dev)
- Fixed a processing bug when the text field got focus.
- Moved some items from the View menu to the File menu.
- Sorted imports.

## Version 6.0.20210224 (dev)
- Removed the View menu.
- Removed some unnecessary files.
- Fixed the process when the menu button is pressed.

## Version 6.0.20210222 (dev)
- Fixed the options panel.

## Version 6.0.20210218 (dev)
- Changed the constant name.
- Removed all blur processing.

## Version 6.0.20210216 (dev)
- Refactored some source files.

## Version 6.0.20210215 (dev)
- Refactored some source files.
- Removed some unnecessary files.
- Adjusted the event handling of the Ace editor.

## Version 6.0.20210211 (dev)
- Removed some options.
- Fixed the options panel.

## Version 6.0.20210210 (dev)
- Fixed a bug where the text field would stick out when opening a CSS or JSON file immediately after launching the app.
- Fixed the screen rewriting process.

## Version 6.0.20210208 (dev)
- Fixed a bug where switching Theme was incomplete.

## Version 6.0.20210206 (dev)
- Updated Material Components for the web to version 10.0.0.
- Changed orientation from landscape-primary to any.

## Version 6.0.20210205 (dev)
- Fixed resources for custom URLs.
- Refactoring of some of the source files.

## Version 6.0.20210204 (dev)
- Adjusted the overlap of Dialog.
- Removed the Drawer header.
- Fixed the function name.
- Fixed to add title attribute of all menu buttons at initialization.

## Version 6.0.20210203 (dev)
- Refactored some source files.
- Fixed language setting.
- Adjusted the overlap of AppBar, Drawer and Statusbar.

## Version 6.0.20210202 (dev)
- Fixed resources for custom URLs.
- Fixed custom scheme.

## Version 6.0.20210201 (dev)
- Fixed the style of statusbar.
- Removed some unnecessary definitions.

## Version 5.2.20210129 (dev)
- Removed unnecessary arguments.

## Version 5.2.20210128 (dev)
- Fixed the behavior of all menus.
- Refactored some source files.

## Version 5.2.20210126 (dev)
- Fixed some title attributes.

## Version 5.2.20210123 (dev)
- Updated Material Components for the web to version 9.0.0.

## Version 5.2.20210122 (dev)
- Changed the initial type of fontSize from String to Number.

## Version 5.2.20210119 (dev)
- Fixed the file list.
- Fixed so as not to create unnecessary elements.

## Version 5.2.20210118 (dev)
- Fixed the title bar.

## Version 5.1.20210115 (dev)
- Improved focus movement when canceling Rename file with just mouse operation.

## Version 5.1.20210114 (dev)
- Removed some unnecessary files at the time of delivery.
- Removed some unnecessary string definitions.

## Version 5.1.20210113 (dev)
- Fixed the welcome message.

## Version 5.1.20210111 (dev)
- Fixed the options panel.
- Fixed the behavior when the editor got focus.
- Fixed individual behavior when closing dialogs.

## Version 5.1.20210107 (dev)
- Removed some unnecessary functions.

## Version 5.1.20210106 (dev)
- Fixed the editor to save some options when it gets focus.

## Version 5.1.20210105 (dev)
- Fixed the options panel.
- Updated the URL list.
- Moved some items from the File menu to the View menu.

## Version 5.1.20210104 (dev)
- Sorted some items in the File menu.
- Removed some items from the File menu.
- Removed some items from the View menu.
- Fixed the options panel.

## Version 5.1.20210101 (dev)
- Fixed the options panel.
- Removed the Extensions menu.

## Version 5.1.20201231 (dev)
- Fixed the options panel.

## Version 5.1.20201230 (dev)
- Fixed the options panel.

## Version 5.1.20201229 (dev)
- Fixed the options panel.

## Version 5.1.20201228 (dev)
- Updated the version number of Ace editor.
- Fixed the options panel.

## Version 5.1.20201218 (dev)
- Changed the name of the menu item from "Show settings menu" to "Preferences".

## Version 5.1.20201216 (dev)
- Added a function to display a link to README.md when the app is started, in case the app cannot be started.

## Version 5.1.20201215 (dev)
- Removed some unused definitions.
- Revised the processing of app_starter.js.

## Version 5.0.20201208 (dev)
- Removed some unnecessary processing.

## Version 5.0.20201207 (dev)
- Removed some unnecessary files.
- Improved the behavior when operating the mouse.
- Fixed to change the command registration process according to the window style.
- Added the process to check the window style.

## Version 5.0.20201204 (dev)
- Removed some unnecessary definitions.

## Version 5.0.20201203 (dev)
- Changed keyboard shortcuts for some features.
- Removed some menu items.
- Rewrote some source code.

## Version 5.0.20201202 (dev)
- Fixed typo.

## Version 5.0.20201201 (dev)
- Fixed the possibility of an error.

## Version 5.0.20201130 (dev)
- Fixed a bug that could not be downloaded.

## Version 5.0.20201129 (dev)
- Improved the operation of File list.

## Version 5.0.20201126 (dev)
- Fixed screen update process.

## Version 5.0.20201125 (dev)
- Added an item to the Help menu to open PERMISSIONS.md.
- Added a link to PERMISSIONS.md.

## Version 5.0.20201124 (dev)
- Fixed to switch the editing area when opening a file that is already open.

## Version 5.0.20201123 (dev)
- Moved all File Picker functionality to the FileHelper class.
- Added support for AbortError in showOpenFilePicker.

## Version 5.0.20201120 (dev)
- Added support for AbortError in showSaveFilePicker.
- Replaced all forEach with for.
- Improved Open file and Save file.
- Formatted try / catch.

## Version 5.0.20201117 (dev)
- Fixed some functions to use the File System Access API.
- Added some functions that use the File System Access API.

## Version 5.0.20201112 (dev)
- Fixed some function names.

## Version 5.0.20201110 (dev)
- Added some functions that use the File System Access API.

## Version 5.0.20201108 (dev)
- Fixed the process to get all options.
- Moved defaults for all options to /res/editor_options.js.

## Version 4.2.20201030.0-u1 (stable)
- Fixed a bug in resource loading.

## Version 5.0.20201104 (dev)
- Fixed a bug in resource loading.
- Updated Material Icons to version 4.0.0.
- Updated Material Components for the web to version 8.0.0.

## Version 4.2.20201030 (dev)
- Fixed some definitions in Edit menu.

## Version 4.2.20201027 (dev)
- Added To lower case to Alt+U.
- Added Move lines up/down to Alt+; and Alt+'.

## Version 4.2.20201026 (dev)
- Revised the processing of some functions.
- Revised the arguments of some functions.
- Removed some unnecessary processing.

## Version 4.2.20201025 (dev)
- Revised the processing of some functions.

## Version 4.2.20201023 (dev)
- Separated the processing of some functions.

## Version 4.2.20201023 (dev)
- Changed elements according to the reference.
- Revised the arguments of some functions.
- Removed some unnecessary functions.
- Fixed some functions of DialogPrompt class.
- Fixed the attributes of some elements in the dialog.

## Version 4.2.20201022 (dev)
- Changed the dialog style.
- Changed some elements from div to label.
- Fixed some functions of DialogPrompt class.
- Fixed some functions of Dialog class.
- Fixed a bug that Close file may not work.

## Version 4.2.20201021 (dev)
- Moved some dialog related files to /public/dialog/.

## Version 4.2.20201020 (dev)
- Moved some dialog related files to /public/dialog/.

## Version 4.2.20201017 (dev)
- Fixed some import declarations in /public/app_starter.js.
- Removed some unnecessary properties.
- Removed some unnecessary functions.

## Version 4.2.20201015 (dev)
- Fixed some dialog processing.
- Removed some unnecessary functions.

## Version 4.2.20201014 (dev)
- Fixed the display of Statusbar.

## Version 4.2.20201013 (dev)
- Fixed the display of New line mode.

## Version 4.2.20201009 (dev)
- Fixed class inheritance.
- Sorted imports.
- Changed some constant names.
- Added categories to the manifest.

## Version 4.2.20201006 (dev)
- Moved some constant definitions to another file.
- Removed some unnecessary string definitions.
- Removed public/res.js.
- Review all imports.
- Separated the definition of public/res.js.

## Version 4.2.20201005 (dev)
- Fixed some dialog processing.
- Fixed an incorrect app cache name.
- Removed some unnecessary processing.
- Fixed the procedure for setting the class attribute.

## Version 4.2.20201002 (dev)
- Fixed the status bar display.

## Version 4.2.20201001 (dev)
- Updated the welcome message.
- Updated ABOUT.md.

## Version 4.1.20200930 (dev)
- Fixed the display of the cursor position.
- Updated the welcome message.

## Version 4.1.20200928 (dev)
- Moved the welcome message from /public/res.js to /public/res/welcome.js.
- Fixed due to app name change.

## Version 4.1.20200924 (dev)
- Refactored core.js and keybinding.js.

## Version 4.1.20200924 (dev)
- Added screenshots to the app manifest.
- Removed some unnecessary escapes.

## Version 4.1.20200923 (dev)
- Removed some unnecessary processing.
- Removed some unnecessary imports.

## Version 4.1.20200922 (dev)
- Sorted imports.

## Version 4.1.20200916 (dev)
- Refactored material_helper.js.
- Added Shortcut list item to Help menu.
- Added a command to display a Shortcut list.

## Version 4.1.20200915 (dev)
- Refactored status_helper.js.
- Changed the display indicating that the text was changed.
- Disable Insert Welcome message when Read-Only.
- Added the file name to the string.

## Version 4.1.20200914 (dev)
- Added a switch to show / hide menu button shortcuts.
- Changed the option name "menuButton" to "menuButtonStyle".

## Version 4.1.20200912 (dev)
- Added keyboard shortcuts to menu button text.

## Version 4.1.20200909 (dev)
- Deleted ABOUT-JP.md.
- Updated ABOUT.md.

## Version 4.1.20200907 (dev)
- Removed comments from options.js.
- Added Privacy policy item to Help menu.

## Version 4.1.20200906 (dev)
- Added a setting to hide Chrome's translation function.

## Version 4.1.20200905 (dev)
- Changed /res/CHANGELOG.md to be displayed on a web browser.
- Fixed a crash that occurs when displaying the Rename file dialog when downloading a new file.
- Removed unnecessary function.

## Version 4.1.20200904 (dev)
- Renamed the await function according to the coding guidelines.

## Version 4.1.20200903 (dev)
- Moved menu button resources to /res/menu_buttons.js.
- Removed uppercase specification of button on top-app-bar.

## Version 4.1.20200831 (dev)
- Changed /res/OPEN_SOURCE_LISENCE.md to be displayed on a web browser.
- Renamed the app name to "Weber Notepad".

## Version 4.1.20200828 (dev)
- Updated the necessary parts along with the move of the development repository.

## Version 4.0.20200827 (dev)
- Added one item to the Help menu.
- Fixed the problem that Find next and Find previous in the Search menu do not work.
- Added one item to the Edit menu.
- Added two items to the Code menu.

## Version 4.0.20200824 (dev)
- Rewrote the service worker again.

## Version 4.0.20200820 (dev)
- Rewrote the service worker.
- Fixed manifest.json.

## Version 4.0.20200819 (dev)
- Updated Material Icons to version 3.0.2.
- Rewrote the service worker with reference to PWABuilder.

## Version 4.0.20200818 (dev)
- Fixed a bug that a confirmation dialog with a problem is displayed when Alt+W is repeatedly pressed during editing.
- Fixed to use Ace's themelist extension to get theme list.
- Fixed the call point of Core class.
- Changed the tab size of index.html from 4 to 2.
- Fixed the link element.

## Version 4.0.20200817 (dev)
- Fixed name and description of manifest.json.
- Renamed MoveDrawerEvent to ChangeDrawerEvent.
- Renamed descriptions to DESCRIPTIONS.
- Removed unnecessary import.
- Changed to dynamic import.

## Version 4.0.20200816 (dev)
- Updated welcome message.

## Version 4.0.20200814 (dev)
- Fixed start_url of manifest.json.

## Version 4.0.20200813 (dev)
- Fixed the message after updating the cache.
- Fixed Snackbar action and dismiss.
- Refactored oed.js.
- Fixed window title string.

## Version 4.0.20200812 (dev)
- Adjusted the height of menu items again.

## Version 4.0.20200811 (dev)
- Removed some unnecessary string definitions.

## Version 4.0.20200810 (dev)
- Improved the behavior of menu buttons.
- Fixed the divider in the menu.
- Adjusted the height of menu items.
- Updated domain name.

## Version 4.0.20200808 (dev)
- Sorted OED file list.
- Fixed window title string.

## Version 4.0.20200806 (dev)
- Deleted ToggleDrawerEvent class.
- Changed ToggleDrawerEvent to MoveDrawerEvent.
- Added MoveDrawerEvent class.
- Added the function to insert Welcome message as many times as you like.
- Improved the welcome message insertion process.
- Improved to reuse the source code window of OED.

## Version 4.0.20200805 (dev)
- Added the function to open the source code of OED to the Help menu.
- Improved the theme update process.
- Updated localForage to version 1.9.0.
- Added res/descriptions.js to the OED file list.
- Removed Copy and Cut from Edit menu.

## Version 4.0.20200804 (dev)
- Moved the definition of descriptions from res.js to res/descriptions.js.
